sap.ui.jsview("de.lsv.pi.message.archive.ui.web.MainView", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	getControllerName : function() {
		return "de.lsv.pi.message.archive.ui.web.MainView";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	createContent : function(oController) {
		var oArchiveList = sap.ui.view( {
			id : "idArchiveListView1",
			viewName : "de.lsv.pi.message.archive.ui.web.ArchiveListView",
			type : sap.ui.core.mvc.ViewType.JS
		});

		var oShell = new sap.ui.ux3.Shell("myShell", {
			appTitle : "Message Archive Access",
			showLogoutButton : false,
			showSearchTool : false,
			showInspectorTool : false,
			showFeederTool : false,
			worksetItems : [ new sap.ui.ux3.NavigationItem("WI_overview", {
				key : "wi_overview",
				text : "Overview"
			}) ],
			content : oArchiveList,
			worksetItemSelected : function(oEvent) {
				var sId = oEvent.getParameter("id");
				var oShell = oEvent.oSource;
				switch (sId) {
				case "WI_overview":
					oShell.setContent(oArchiveList);
					break;
				default:
					break;
				}
			}
		});
		return oShell;
//		return [];
	}

});
