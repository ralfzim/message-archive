sap.ui.controller("de.lsv.pi.message.archive.ui.web.ArchiveListView", {

	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	REFERENCES : {},
	onInit : function() {
	},

	setControls : function(sId, oControl) {
		this.REFERENCES[sId] = oControl;
	},
	/**
	 * Similar to onAfterRendering, but this hook is invoked before the
	 * controller's View is re-rendered (NOT before the first rendering!
	 * onInit() is used for that one!).
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	// onBeforeRendering: function() {
	//
	// },
	/**
	 * Called when the View has been rendered (so its HTML is part of the
	 * document). Post-rendering manipulations of the HTML could be done here.
	 * This hook is the same one that SAPUI5 controls get after being rendered.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	// onAfterRendering: function() {
	//
	// },
	/**
	 * Called when the Controller is destroyed. Use this one to free resources
	 * and finalize activities.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */

	onExit : function() {

	},

	createSaveStatusPopup : function() {
		var oDialog = sap.ui.xmlfragment(
				"de.lsv.pi.message.archive.ui.web.status_popup", this);
		oDialog.setTitle("SaveMessage Status Report");
		oDialog.setModal(true);
		oDialog.setShowCloseButton(false);
		oDialog.setWidth("700px");
		return oDialog;
	},
	_oStatusDialog : null,

	showSaveStatus : function(view, res) {
		console.log(res);
		var self = this;
		var oStatusContext = {
			processID : res.processUUID,
			status : "starting...",
			enabled : false
		};
		var oModel = new sap.ui.model.json.JSONModel(oStatusContext);
		if (!this._oStatusDialog) {
			this._oStatusDialog = this.createSaveStatusPopup();
		}
		this.getView().addDependent(this._oStatusDialog);
		this._oStatusDialog.setModel(oModel);
		this._oStatusDialog.getButtons()[0].setEnabled(false);
		this._oStatusDialog.open();
		var counter = 0;
		var timeout = function() {
			function success(data) {
				if (data.evaluationState == 'NoFile') {
					setTimeout(timeout,15000);
					counter = 0;
				}else if (data.evaluationState == 'OK' && data.actualAmount < data.maxAmount) {
					oStatusContext.status = "" + data.actualAmount + "/" + data.maxAmount;
					oModel.setData(oStatusContext);
					setTimeout(timeout,15000);
					counter = 0;
				} else if (data.evaluationState == 'OK' ) {
					oStatusContext.status = "" + data.actualAmount + "/" + data.maxAmount;
					oModel.setData(oStatusContext);
					self._oStatusDialog.getButtons()[0].setEnabled(true);
				} else if (counter < 100) {
					counter++;
					setTimeout(timeout,15000);
				} else {
					oStatusContext.status = "Error on server: " + data.evaluationState + ". I give up after " + counter +" retries.";
				}
			}
			$.LSV.saveMessagesStatus({
				path : res.path,
				processID : res.processUUID
			},success,console.log)
		}
		setTimeout(timeout,15000);
	},

	onStatusDlgConfirm : function() {
		this._oStatusDialog.close();
	}

});