sap.ui.jsview("de.lsv.pi.message.archive.ui.web.ArchiveListView", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	getControllerName : function() {
		return "de.lsv.pi.message.archive.ui.web.ArchiveListView";
	},
	_References : {},
	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf com.sap.cid.zsism.pi.receiver.ui.MainView
	 */
	aSearchFields : [
        			new sap.ui.core.ListItem({id:"ITN", text:"ITN", additionalText:"Interface Name"}),
         			new sap.ui.core.ListItem({id:"ITU", text:"ITU", additionalText:"Interface Namespace"}),
         			new sap.ui.core.ListItem({id:"DTS", text:"DTS", additionalText:"Sending Date"}),
         			new sap.ui.core.ListItem({id:"SSP", text:"SSP", additionalText:"Sender Party"}),
         			new sap.ui.core.ListItem({id:"RSP", text:"RSP", additionalText:"Receiver Party"}),
         			new sap.ui.core.ListItem({id:"SSN", text:"SSN", additionalText:"Sender Service"}),
//         			new sap.ui.core.ListItem({id:"City6", text:"New York", additionalText:"USA"}),
//         			new sap.ui.core.ListItem({id:"City6", text:"New York", additionalText:"USA"}),
//         			new sap.ui.core.ListItem({id:"City6", text:"New York", additionalText:"USA"}),
         			new sap.ui.core.ListItem({id:"RSN", text:"RSN", additionalText:"Receiver Service"})]
         			,
    aSearchOperators : [
         		    new sap.ui.core.ListItem({id:"EQ", text:"EQ", additionalText:"equals"}),
         		    new sap.ui.core.ListItem({id:"LT", text:"LT", additionalText:"lower than"}),
         		    new sap.ui.core.ListItem({id:"GT", text:"GT", additionalText:"greater than"}),
         		    new sap.ui.core.ListItem({id:"LE", text:"LE", additionalText:"lower than or equals"}),
         		    new sap.ui.core.ListItem({id:"GE", text:"GE", additionalText:"greater than or equals"}),
         		    new sap.ui.core.ListItem({id:"BT", text:"BT", additionalText:"between"})]
         		    ,
    aSearchOptions : [
         		    new sap.ui.core.ListItem({id:"I", text:"I", additionalText:"Include"}),
         		    new sap.ui.core.ListItem({id:"E", text:"E", additionalText:"Exclude"})]
         		    ,
    createSearchPanelCombo : function(oTable,aValues,sTitle,sBind,iWidth) {
		var oListBox = new sap.ui.commons.ListBox("ListBox"+sTitle, {items : aValues});
		var oColumn = new sap.ui.table.Column( {
			label : new sap.ui.commons.Label( {
				text : sTitle
			}),
			template : new sap.ui.commons.ComboBox("ComboBox"+sTitle, {tooltip:"City", displaySecondaryValues:true, "association:listBox" : oListBox}).bindProperty("value",sBind),
			sortProperty : sBind,
			filterProperty : sBind,
			width : iWidth
		});
		oTable.addColumn(oColumn);
	},
	initSearchModel : function() {
		return [{
			field : 'SSN',
			option: 'I',
			low: '',
			high: '',
			operator : 'EQ',
			id : 0
		}];
	}, 
	handlePaginator: function(data) {
		var oPaginator = this._References.paginator;
		var startPos = data.startRange;
	    if (data.startRange === null) {
	       this._References.paginatorConfig = {
	       	   numberOfPages : 0,
	       	   pageRange: 100,
	       	   currentPage: 1
	       };
	    } else if (startPos == 0) {
		  var length = data.length || 0;
		  var endPos = data.endRange || 100;
		  var range = endPos - startPos;
		  if (range == 0) { range = 100; }
	       this._References.paginatorConfig = {
	       	   numberOfPages : Number.parseInt(length/range),
	       	   pageRange: range,
	       	   currentPage: 1
	       };
	    }
    	  oPaginator.setNumberOfPages(this._References.paginatorConfig.numberOfPages);
  		  oPaginator.setCurrentPage(this._References.paginatorConfig.currentPage);
	
	},
	setResultTable: function(data) {
		var oSearchResult = this._References.searchResult;
		var oModel = new sap.ui.model.json.JSONModel();
		this._References.searchResult = oModel;
		var oTable = this._References.resultTable;
		data.messages.forEach(function(i){
			i.linkText = i.messageId;
			i.href = $.LSV.extractUrl(i.messageId);
		});
		oModel.setData(data);
		oTable.setModel(oModel);
		oTable.bindRows("/messages");
		try { this.handlePaginator(data) } catch(e) {}
		oTable.setBusy(false);
	},
	handleLoadError: function(err) {
	    console.log(err);
		this.setResultTable({messages : []});
	},
	searchMessages : function() {
	    var oTable = this._References.resultTable;
	    oTable.setBusy(true);
		var oSearchRequest = this._References.searchModel;
		var oSearchResult = this._References.searchResult;
		var aSearch = [];
		var oData = oSearchRequest.getData();
			oData.criteria.forEach(function(i){
			if (i.low !== '') 
				aSearch.push({
					operator : i.operator,
					type : i.option,
					field : i.field,
					low : i.low,
					high : i.high
				});
		});
		console.log(aSearch);
		this._References.lastSearch = aSearch;
		$.LSV.searchEntries({criterions: aSearch},this.setResultTable.bind(this),this.handleLoadError.bind(this));
	},
	onpage : function(pagenum) {
	    var oTable = this._References.resultTable;
	    oTable.setBusy(true);
	    console.log("Page: " + pagenum);
	    var oConfig = this._References.paginatorConfig;
	    oConfig.currentPage = pagenum.mParameters.targetPage;
	    this._References.paginatorConfig = oConfig;
		$.LSV.searchEntries({ criterions: this._References.lastSearch,
							  start: (oConfig.currentPage-1) * oConfig.pageRange,
							  length: oConfig.pageRange
							},
							this.setResultTable.bind(this),
							this.handleLoadError.bind(this));
	},
	saveMessages : function() {
		var self = this;
		var oSearchRequest = this._References.searchModel;
		var oSearchResult = this._References.searchResult;
		var aSearch = [];
		var oData = oSearchRequest.getData();
			oData.criteria.forEach(function(i){
			if (i.low !== '') 
				aSearch.push({
					operator : i.operator,
					type : i.option,
					field : i.field,
					low : i.low,
					high : i.high
				});
		});
		console.log(aSearch);
		var oDialog1 = new sap.ui.commons.Dialog();
		oDialog1.setTitle("Save Messages");
		var oPathField = new sap.ui.commons.TextField({value: "",
			layoutData: new sap.ui.core.VariantLayoutData({
				multipleLayoutData: [new sap.ui.layout.ResponsiveFlowLayoutData({weight: 4}),
	                  	     new sap.ui.layout.form.GridElementData({hCells: "10"})]
				})
		});
		var oLayout1 = new sap.ui.layout.form.GridLayout();
		var oForm1 = new sap.ui.layout.form.Form({
//			title: new sap.ui.core.Title({text: "Address Data", tooltip: "Title tooltip"}),
			layout: oLayout1,
			width: "700px",
			formContainers: [
			 new sap.ui.layout.form.FormContainer({
				title: "Path on server to extract",
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text:"Path",
							layoutData: new sap.ui.core.VariantLayoutData({
								multipleLayoutData: [new sap.ui.layout.ResponsiveFlowLayoutData({weight: 1}),
					                  	     new sap.ui.layout.form.GridElementData({hCells: "2"})]
								})
							}),
						fields: [oPathField	],
						layoutData: new sap.ui.layout.ResponsiveFlowLayoutData({linebreak: true, margin: false})
					}),
				               
				]
			 })	
			]
		});
		var oText = new sap.ui.commons.TextView({value: "Hello World!"});
		oDialog1.addContent(oForm1);
		oDialog1.addButton(new sap.ui.commons.Button({text: "OK", press:function(){
			var sPath = oPathField.getValue();
			if (sPath !== undefined && sPath !== ""){
				$.LSV.saveEntries({criterions: aSearch,pathToSave : sPath},self.controller.showSaveStatus.bind(self.controller,self),console.log);
			}
			oDialog1.close();
		}}));
		oDialog1.open();
	},
	createSearchPanel : function(oController,aControls)   {
		var aSearchModel = this.initSearchModel();
		var iIdCounter = 1;
		var oModel = new sap.ui.model.json.JSONModel();
		
		this._References.searchModel = oModel;
		var oPanel = new sap.ui.commons.Panel({width: "inherit"});

		// Set the title of the panel
		oPanel.setTitle(new sap.ui.core.Title({text: "Search Criteria"}));
		
		var oTable = new sap.ui.table.Table( {
			title : "Query",
			visibleRowCount : 5,
			width : 'auto',
			selectionMode : sap.ui.table.SelectionMode.Single
		});
		this.createSearchPanelCombo(oTable,this.aSearchFields,"Field","field","50px");
		this.createSearchPanelCombo(oTable,this.aSearchOperators,"Operator","operator","50px");
		this.createSearchPanelCombo(oTable,this.aSearchOptions,"","option","10px");
		oTable.addColumn(new sap.ui.table.Column({
			label: new sap.ui.commons.Label({text: "Low"}),
			template: new sap.ui.commons.TextField().bindProperty("value", "low"),
			sortProperty: "low",
			filterProperty: "low",
			width: "60px"
		}));
		oTable.addColumn(new sap.ui.table.Column({
			label: new sap.ui.commons.Label({text: "High"}),
			template: new sap.ui.commons.TextField().bindProperty("value", "high"),
			sortProperty: "high",
			filterProperty: "high",
			width: "60px"
		}));
		oTable.addColumn(new sap.ui.table.Column({
			label: new sap.ui.commons.Label({text: "Remove"}),
			template: new sap.ui.commons.Button({
				text : "X",
				press: function(data) {
				   var oCtxt = data.getSource().getBindingContext();
				   var oEntry = oCtxt.getObject();
				   aSearchModel = aSearchModel.filter(function(i) {return i.id !== oEntry.id;});
				   oModel.setData({criteria: aSearchModel});
				}
			}),
			width: "10px"
		}));
		
		function createRow() {
			
			aSearchModel.push({
			field : 'RSN',
			option: 'I',
			low: '',
			high: '',
			operator : 'EQ',
			id : iIdCounter++
		});
			oModel.setData({criteria: aSearchModel});
		}
	
		function clearTable() {
			aSearchModel = this.initSearchModel();
			oModel.setData({criteria: aSearchModel});
			iIdCounter = 1;
		}
		// Add some buttons to the panel header
		var oAddBtn = new sap.ui.commons.Button({
				text: 'Add',
				press: createRow
			});
		var oClearBtn = new sap.ui.commons.Button({
				text: 'Clear',
				press: clearTable.bind(this)
			});
		oPanel.addButton(new sap.ui.commons.Button({
			text: 'Search',
			press: this.searchMessages.bind(this)
		}));
		oTable.setToolbar(new sap.ui.commons.Toolbar( {
			items : [ oClearBtn ,oAddBtn]
		}));

		oPanel.addContent(oTable);
		aControls.push(oPanel);
		oModel.setData({criteria: aSearchModel});
		oTable.setModel(oModel);
		oTable.bindRows("/criteria");

	},
	
	createContent : function(oController) {
		this.controller = oController;
		var aControls = [];
		var iIdCounter = 0;
		var aSearchEntries = [];

		this.createSearchPanel(oController, aControls);
		
		
		var oTable = new sap.ui.table.Table( {
			title : "History",
			visibleRowCount : 10,
			width : 'auto',
			selectionMode : sap.ui.table.SelectionMode.Single,
			enableColumnReordering: true,
			showColumnVisibilityMenu: true
		});
		this._References.resultTable = oTable;
		function addColumn(sTitle,sBind,bVisible) {
			var oColumn = new sap.ui.table.Column( {
					label : new sap.ui.commons.Label( {
					text : sTitle
				}),
				template : new sap.ui.commons.TextView().bindProperty("text",
					sBind),
				sortProperty : sBind,
				filterProperty : sBind,
				width: "30px"				
			});
			oTable.addColumn(oColumn);
		}
		oTable.addColumn(new sap.ui.table.Column({
			label: new sap.ui.commons.Label({text: "Message Id"}),
			template: new sap.ui.commons.Link().bindProperty("text", "linkText").bindProperty("href", "href"),
			sortProperty: "linkText",
			filterProperty: "linkText",
			width: "20px"
		}));
//		addColumn("Message Id","messageId",false);
		addColumn("Interface","msgInterface/interfaceName",true);
		addColumn("Namespace", "msgInterface/namespaceURI",true);
		addColumn("Sender", "senderService/name",true);
		addColumn("Sender Party", "senderService/party",false);
		addColumn("Receiver", "receiverService/name",true);
		addColumn("Receiver Party", "receiverService/party",false);
		addColumn("Sending Date", "sendingDate",false);
		addColumn("Deletion Date", "deletionDate",false);
		var oBtnClear = new sap.ui.commons.Button( {
			text : "Export",
			press :	this.saveMessages.bind(this)
		});
		oTable.setToolbar(new sap.ui.commons.Toolbar( {
			items : [ oBtnClear ]
		}));
		oController.setControls('idHistory',oTable);
		aControls.push(oTable);
		var oPaginator = new sap.ui.commons.Paginator("resultPaginator", {numberOfPages:0,currentPage:1,page:this.onpage.bind(this)});
		this._References.paginator = oPaginator;
		aControls.push(oPaginator);
		return aControls;
	}

});
