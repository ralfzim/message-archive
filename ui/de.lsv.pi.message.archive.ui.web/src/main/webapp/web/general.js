(function($) {
	if ($.LSV) {
		return;
	}
	var OPTIONS = {
//		baseUrl :  '/de.lsv.pi.message.archive.ui.web/proxy/http/sapp1e.rz.lsv.de:50100/LSVArchive'
		baseUrl :  '/LSVArchive'
	}
	$['LSV'] = {
		setBaseUrl : function(url) {
			OPTIONS.baseUrl = url;
		},
		getBaseUrl : function() {
			return OPTIONS.baseUrl;
		},
		searchEntries : function(oSearch, suc, err) {
			var sJson = JSON.stringify(oSearch);
			console.log(sJson);
			$.ajax(OPTIONS.baseUrl + '/Messages', {
				success : suc,
				error : err,
				data : sJson,
				dataType : 'json',
				processData : true,
				type : 'POST'
			});
		},
		saveEntries : function(oSearch, suc, err) {
			var sJson = JSON.stringify(oSearch);
			console.log(sJson);
			$.ajax(OPTIONS.baseUrl + '/SaveMessages', {
				success : suc,
				error : err,
				data : sJson,
				dataType : 'json',
				processData : true,
				type : 'POST'
			});
		},
		saveMessagesStatus : function(oSearch,suc,err) {
			var sJson = JSON.stringify(oSearch);
			console.log(sJson);
			$.ajax(OPTIONS.baseUrl + '/SaveMessagesStatus', {
				success : suc,
				error : err,
				data : sJson,
				dataType : 'json',
				processData : true,
				type : 'POST'
			});
		},
		extractUrl: function(sMsgId) {
			return OPTIONS.baseUrl + "/ExtractMessage/"+sMsgId;
		}
	};
})(jQuery);