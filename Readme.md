Message Archive
=================

Diese J2EE Applikation stellt ein Adapter Module bereit, welches die Nachrichten und deren Inhalt
in eine Datenbank speichert. Au�erdem gibt es noch eine WebApplikation, mit der die Nachrichten im 
Archive selektiert werden k�nnen.


Adapter Module
-----------------

Der JNDI Name ist LSV_ArchiveMessage.

Das Module kennt folgende Parameter:

 Name                   |   Beschreibung                                                       
------------------------|----------------------------------------------------------------------
 retention.period.anno  | Aufbewahrungsfrist in Jahren                                                                     
 retention.period.days  | Aufbewahrungsfrist in Tagen                                                                     



WebApplikation
----------------

- http://<host>:<port>/LSVArchiveUI/index.html
- https://<host>:<port>/LSVArchiveUI/index.html


