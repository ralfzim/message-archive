package de.lsv.pi.message.archive.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersistContentKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "LEAST")
	private long least;
	@Column(name = "MOST")
	private long most;

	private short linenr;
	public long getLeast() {
		return least;
	}

	public void setLeast(long least) {
		this.least = least;
	}

	public long getMost() {
		return most;
	}

	public void setMost(long most) {
		this.most = most;
	}

	public PersistContentKey() {
		this(0L, 0L);
	}

	public PersistContentKey(long least, long most) {
		super();
		this.least = least;
		this.most = most;
	}

	@Override
	public String toString() {
		return "Key [least=" + least + ", most=" + most + "]";
	}

	public void setLinenr(short linenr) {
		this.linenr = linenr;
	}

	public short getLinenr() {
		return linenr;
	}

}
