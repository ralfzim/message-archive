package de.lsv.pi.message.archive.db;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.db.query.SearchStringBuilder;
import de.lsv.pi.message.archive.db.query.SearchStringBuilder.Field;

public class ResolveDimension {
	
	private final static Location loc = Location.getLocation(ResolveDimension.class);

	private EntityManager entityManager;
	private List<Dimension> unresolvedDimension;
	private final static String SQL_STMT = "SELECT m.* FROM MSG_ARCHIVE m WHERE m.%s = %d";
	private Map<Dimension, List<PersistMessage>> cache = new HashMap<Dimension, List<PersistMessage>>();
	private Field field;
	private int maxSize;
	private Dimension maxSizeDimension;
	private DimensionAccess dimensionAccess;

	public ResolveDimension(EntityManager em, List<Dimension> resultList) {
		loc.entering("constructor",new Object[]{resultList});
		this.entityManager = em;
		this.unresolvedDimension = resultList;
		if (this.unresolvedDimension.size() < 2)
			throw new RuntimeException();
		findField();
		loc.exiting("constructor");
	}

	private void findField() {
		loc.entering("findField");
		Iterator<Dimension> it = this.unresolvedDimension.iterator();
		String type = it.next().getType();
		this.field = SearchStringBuilder.Field.valueOf(type);
		switch(this.field) {
		case DTD:
		case DTS:
		case KEY:
			throw new RuntimeException();
		case ITN:
			this.dimensionAccess = DimensionAccess.senderInterfaceName;
			break;
		case ITU:
			this.dimensionAccess = DimensionAccess.senderInterfaceURI;
			break;
		case RSN:
			this.dimensionAccess = DimensionAccess.receiverServiceName;
			break;
		case RSP:
			this.dimensionAccess = DimensionAccess.receiverServiceParty;
			break;
		case SSN:
			this.dimensionAccess = DimensionAccess.senderServiceName;
			break;
		case SSP:
			this.dimensionAccess = DimensionAccess.senderServiceParty;
			break;
		}
		loc.exiting("findField",this.dimensionAccess);
	}

	public Dimension process() {
		loc.entering("process");
		findResults();
		prepareUpdate();
		processUpdate();
		removeUnusedDimension();
		close();
		loc.exiting("process",this.maxSizeDimension);
		return this.maxSizeDimension;
	}

	private void close() {
		loc.entering("close");
		this.cache.clear();
		this.cache = null;
		loc.exiting("close");
	}

	private void removeUnusedDimension() {
		loc.entering("removeUnusedDimension");
		loc.debugT("removing duplicate dimension {0}",new Object[]{this.cache.entrySet()});
		for (Dimension dimension : this.cache.keySet()) {
			this.entityManager.remove(dimension);
		}
		loc.exiting("removeUnusedDimension");
	}

	private void processUpdate() {
		loc.entering("processUpdate");
		for (List<PersistMessage> e : this.cache.values()) {
			for (PersistMessage persistMessage : e) {
				this.dimensionAccess.setDimension(persistMessage, maxSizeDimension);
				this.entityManager.merge(persistMessage);
			}
		}
		loc.exiting("processUpdate");
	}

	private void prepareUpdate() {
		loc.entering("prepareUpdate");
		this.cache.remove(this.maxSizeDimension);
		loc.exiting("prepareUpdate");
	}

	private void findResults() {
		loc.entering("findResults");
		for (Dimension d : this.unresolvedDimension) {
			String query = String.format(SQL_STMT, this.field.getFieldName(), d.getId());
			loc.debugT("jpa stmt {0} for dimenstion {1}",new Object[]{query,d});
			@SuppressWarnings("unchecked")
			List<PersistMessage> result = (List<PersistMessage>) this.entityManager //
					.createNativeQuery(query, PersistMessage.class)//
					.getResultList();
			this.cache.put(d, result);
			loc.debugT("results {0} for dimenstion {1}",new Object[]{result.size(),d});
			if (result.size() > this.maxSize) {
				this.maxSize = result.size();
				this.maxSizeDimension = d;
			}
		}
		loc.exiting("findResults");
	}

}
