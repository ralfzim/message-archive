package de.lsv.pi.message.archive.db.util;

import java.io.File;

public class PathProvider {

	private File baseDir;
	private long index;
	private String path;

	public PathProvider(File baseDir,long index) {
		this.baseDir = baseDir;
		this.path = buildPath();
		this.index = index;
	}

	public PathProvider(File file) {
		this(file,0);
	}

	public boolean hasNewPath() {
		index++;
		if ((index % 100) == 0) {
			this.path = buildPath();
			return true;
		} else
			return false;
	}

	private String buildPath() {
		String addPath = String.format("%08d", (index / 100));
		addPath = addPath.replaceAll("(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1/$2/$3/$4");
		return new File(baseDir, addPath).toString();
	}

	public String getPathAsString() {
		return this.path;
	}
	public File getBaseDir() {
		return this.baseDir;
	}
}
