package de.lsv.pi.message.archive.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MSG_ARCHIVE")
public class PersistMessageRead implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PersistMessageKey key;

	@ManyToOne
	@JoinColumn(name="SENDERSERVICEPARTY")
	@Column(name="SENDERSERVICEPARTY")
	private Dimension senderServiceParty;
	
	@ManyToOne
	@JoinColumn(name="RECEIVERSERVICEPARTY")
	@Column(name="RECEIVERSERVICEPARTY")
	private Dimension receiverServiceParty;

	@ManyToOne
	@JoinColumn(name="SENDERSERVICENAME")
	@Column(name="SENDERSERVICENAME")
	private Dimension senderServiceName;

	@ManyToOne
	@JoinColumn(name="RECEIVERSERVICENAME")
	@Column(name="RECEIVERSERVICENAME")
	private Dimension receiverServiceName;
	
	@ManyToOne
	@JoinColumn(name="INTERFACEURI")
	@Column(name="INTERFACEURI")
	private Dimension senderInterfaceURI;
	
	@ManyToOne
	@JoinColumn(name="INTERFACENAME")
	@Column(name="INTERFACENAME")
	private Dimension senderInterfaceName;
	
	@Column(name="SENDINGDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendingDate;
	@Column(name="DELETIONDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletionDate;
//	@Lob 
//	private String content;
	public PersistMessageKey getKey() {
		return key;
	}
	public void setKey(PersistMessageKey key) {
		this.key = key;
	}
	public Dimension getSenderServiceParty() {
		return senderServiceParty;
	}
	public void setSenderServiceParty(Dimension senderServiceParty) {
		this.senderServiceParty = senderServiceParty;
	}
	public Dimension getReceiverServiceParty() {
		return receiverServiceParty;
	}
	public void setReceiverServiceParty(Dimension receiverServiceParty) {
		this.receiverServiceParty = receiverServiceParty;
	}
	public Dimension getSenderServiceName() {
		return senderServiceName;
	}
	public void setSenderServiceName(Dimension senderServiceName) {
		this.senderServiceName = senderServiceName;
	}
	public Dimension getReceiverServiceName() {
		return receiverServiceName;
	}
	public void setReceiverServiceName(Dimension receiverServiceName) {
		this.receiverServiceName = receiverServiceName;
	}
	public Dimension getSenderInterfaceURI() {
		return senderInterfaceURI;
	}
	public void setSenderInterfaceURI(Dimension senderInterfaceURI) {
		this.senderInterfaceURI = senderInterfaceURI;
	}
	public Dimension getSenderInterfaceName() {
		return senderInterfaceName;
	}
	public void setSenderInterfaceName(Dimension senderInterfaceName) {
		this.senderInterfaceName = senderInterfaceName;
	}
	public Date getSendingDate() {
		return sendingDate;
	}
	public void setSendingDate(Date sendingDate) {
		this.sendingDate = sendingDate;
	}
	public Date getDeletionDate() {
		return deletionDate;
	}
	public void setDeletionDate(Date deletionDate) {
		this.deletionDate = deletionDate;
	}
	public String getContent() {
//		return content;
		return "";
	}
	public void setContent(String content) {
//		this.content = content;
	}
	@Override
	public String toString() {
		return "PersistMessage [key=" + key + ", deletionDate=" + deletionDate
				+ ", receiverServiceName=" + receiverServiceName
				+ ", receiverServiceParty=" + receiverServiceParty
				+ ", senderInterfaceName=" + senderInterfaceName
				+ ", senderInterfaceURI=" + senderInterfaceURI
				+ ", senderServiceName=" + senderServiceName
				+ ", senderServiceParty=" + senderServiceParty
				+ ", sendingDate=" + sendingDate + "]";
	}
	
}
