package de.lsv.pi.message.archive.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MSG_ARCHIVE")
public class PersistMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PersistMessageKey key;

	@Column(name="SENDERSERVICEPARTY")
	private int senderServiceParty;
	
	@Column(name="RECEIVERSERVICEPARTY")
	private int receiverServiceParty;

	@Column(name="SENDERSERVICENAME")
	private int senderServiceName;

	@Column(name="RECEIVERSERVICENAME")
	private int receiverServiceName;
	
	@Column(name="INTERFACEURI")
	private int interfaceURI;
	
	@Column(name="INTERFACENAME")
	private int interfaceName;
	
	@Column(name="SENDINGDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendingDate;
	@Column(name="DELETIONDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletionDate;

	public PersistMessageKey getKey() {
		return key;
	}
	public void setKey(PersistMessageKey key) {
		this.key = key;
	}
	public int getSenderServiceParty() {
		return senderServiceParty;
	}
	public void setSenderServiceParty(int senderServiceParty) {
		this.senderServiceParty = senderServiceParty;
	}
	public int getReceiverServiceParty() {
		return receiverServiceParty;
	}
	public void setReceiverServiceParty(int receiverServiceParty) {
		this.receiverServiceParty = receiverServiceParty;
	}
	public int getSenderServiceName() {
		return senderServiceName;
	}
	public void setSenderServiceName(int senderServiceName) {
		this.senderServiceName = senderServiceName;
	}
	public int getReceiverServiceName() {
		return receiverServiceName;
	}
	public void setReceiverServiceName(int receiverServiceName) {
		this.receiverServiceName = receiverServiceName;
	}
	public int getInterfaceURI() {
		return interfaceURI;
	}
	public void setInterfaceURI(int interfaceURI) {
		this.interfaceURI = interfaceURI;
	}
	public int getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(int interfaceName) {
		this.interfaceName = interfaceName;
	}
	public Date getSendingDate() {
		return sendingDate;
	}
	public void setSendingDate(Date sendingDate) {
		this.sendingDate = sendingDate;
	}
	public Date getDeletionDate() {
		return deletionDate;
	}
	public void setDeletionDate(Date deletionDate) {
		this.deletionDate = deletionDate;
	}
	@Override
	public String toString() {
		return "PersistMessage [key=" + key + ", senderServiceParty="
				+ senderServiceParty + ", receiverServiceParty="
				+ receiverServiceParty + ", senderServiceName="
				+ senderServiceName + ", receiverServiceName="
				+ receiverServiceName + ", interfaceURI=" + interfaceURI
				+ ", interfaceName=" + interfaceName + ", sendingDate="
				+ sendingDate + ", deletionDate=" + deletionDate + "]";
	}
	
}
