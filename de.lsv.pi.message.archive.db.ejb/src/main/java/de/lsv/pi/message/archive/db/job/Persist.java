/**
 * 
 */
package de.lsv.pi.message.archive.db.job;

import java.util.UUID;

import javax.persistence.EntityManager;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.db.PersistMessage;
import de.lsv.pi.message.archive.db.PersistMessageKey;
import de.lsv.pi.message.archive.impl.Trace;

class Persist {
	private static final String SAVE = "save";
	private final static Location loc = Location.getLocation(Persist.class);
	private EntityManager em;

	public Persist(EntityManager em2) {
		this.em = em2;
	}

	public void save(Message m) {
		loc.entering(SAVE);
		boolean update;
		PersistMessageKey key = createKey(m.getMessageId());
		PersistMessage pm = em.find(PersistMessage.class, key);
		if (pm != null) {
			update = true;
			loc.debugT(SAVE, "found allways message {0}", Trace.toObject(pm));
		} else {
			update = false;
			pm = new PersistMessage();
			pm.setKey(key);
		}
		pm.setDeletionDate(m.getDeletionDate());
		pm.setSendingDate(m.getSendingDate());
		if (update) {
			em.merge(pm);
			loc.debugT(SAVE, "merge msg {0}", Trace.toObject(pm));
		} 
		em.flush();
		loc.exiting(SAVE);
	}


	private PersistMessageKey createKey(UUID messageId) {
		loc.entering("createKey");
		return new PersistMessageKey(messageId.getLeastSignificantBits(),
				messageId.getMostSignificantBits());
	}
}