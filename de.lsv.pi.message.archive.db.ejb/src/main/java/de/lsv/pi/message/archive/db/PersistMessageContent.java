package de.lsv.pi.message.archive.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="MSG_CONTENT")
@NamedQueries({
	@NamedQuery(name="PersistMessageContent.findByKey",
			query="SELECT c FROM PersistMessageContent c WHERE c.key.most = :most AND c.key.least = :least ORDER BY c.key.linenr")
})
public class PersistMessageContent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PersistContentKey key;

	@Lob
	@Column(name="CONTENT")
	private String content;

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public PersistContentKey getKey() {
		return key;
	}
	public void setKey(PersistContentKey key) {
		this.key = key;
	}

}
