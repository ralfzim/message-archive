/**
 * 
 */
package de.lsv.pi.message.archive.impl;

import java.util.UUID;

import javax.persistence.EntityManager;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.db.PersistMessage;
import de.lsv.pi.message.archive.db.PersistMessageKey;

class Remove {
	private static final String SAVE = "save";
	private final static Location loc = Location.getLocation(Remove.class);
	private EntityManager em;

	public Remove(EntityManager em2) {
		this.em = em2;
	}

	public void save(Message m) {
		loc.entering(SAVE);
		PersistMessageKey key = createKey(m.getMessageId());
		PersistMessage pm = em.find(PersistMessage.class, key);
		em.remove(pm);
		loc.debugT(SAVE, "removed msg {0}", Trace.toObject(pm));
		em.flush();
		loc.exiting(SAVE);
	}


	private PersistMessageKey createKey(UUID messageId) {
		loc.entering("createKey");
		return new PersistMessageKey(messageId.getLeastSignificantBits(),
				messageId.getMostSignificantBits());
	}
}