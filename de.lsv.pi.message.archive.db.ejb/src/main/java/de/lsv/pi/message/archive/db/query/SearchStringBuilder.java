package de.lsv.pi.message.archive.db.query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import de.lsv.pi.message.archive.query.AbstractRequest;
import de.lsv.pi.message.archive.query.Criterion;

public class SearchStringBuilder {
	private StringBuilder stringBuilder;

	public static SearchStringBuilder createSelect() {
		return new SearchStringBuilder();
	}

	public static SearchStringBuilder createDelete() {
		return new SearchStringBuilder("DELETE");
	}

	public SearchStringBuilder() {
		stringBuilder = new StringBuilder("SELECT m.* FROM MSG_ARCHIVE m ");
	}

	public SearchStringBuilder(String p) {
		stringBuilder = new StringBuilder(p).append(" FROM MSG_ARCHIVE m");
	}

	private final static String DF_TO_ORACLE = "yyyyMMddHHmmss";
	private final static String[] DF_INPUTS = { //
	"yyyyMMddHHmmss", //
	"yyyyMMddHHmm", //
	"yyyyMMddHH", //
			"yyyyMMdd", //
	};

	public enum Field {
		SSP("SSP", "senderServiceParty", "Sender Party"), //
		RSP("RSP", "receiverServiceParty", "Receiver Party"), //
		SSN("SSN", "senderServiceName", "Sender"), //
		RSN("RSN", "receiverServiceName", "Receiver"), //
		ITU("ITU", "InterfaceURI", "Namespace"), //
		ITN("ITN", "InterfaceName", "Interface"), //
		KEY("KEY", "", "Message Id") {
			@Override
			public String getCondition(Operator op, Criterion c) {
				StringBuilder b = new StringBuilder();
				UUID uid = UUID.fromString(c.getLow());
				b.append("( MOST ").append(op.opString).append(" ")
						.append(uid.getMostSignificantBits());
				b.append(" AND LEAST ").append(op.opString).append(" ")
						.append(uid.getLeastSignificantBits());
				b.append(" ) ");
				return b.toString();
			}

		},
		DTS("DTS", "SENDINGDATE", "Sending Date") {
			@Override
			protected String prepareString(String s) {
				return prepareDate(s);
			}

			@Override
			public String getCondition(Operator op, Criterion c) {
				return getDateCondition(op, c);
			}

		},
		DTD("DTD", "DELETIONDATE", "Deletion Date") {
			@Override
			protected String prepareString(String s) {
				return prepareDate(s);
			}

			@Override
			public String getCondition(Operator op, Criterion c) {
				return getDateCondition(op, c);
			}
		},
		;

		private Field(String type, String fieldName, String description) {
			this.type = type;
			this.fieldName = fieldName.toUpperCase();
			this.setDescription(description);
		}

		private String type, fieldName, description;

		public String getFieldName() {
			return this.fieldName;
		}

		protected String getDateCondition(Operator op, Criterion c) {
			return op.asSqlString(c, this);
		}

		public String getCondition(Operator op, Criterion c) {
			StringBuilder b = new StringBuilder();
			b.append(" in ");
			b.append("( SELECT d.DIM_ID FROM MSG_DIMENSION d WHERE d.type = '");
			b.append(this.type).append("' and d.value ")
					.append(op.asSqlString(c, this)).append(") ");
			return b.toString();
		}

		protected String prepareString(String s) {
			if (s == null || s.length() == 0)
				return "'\\'";
			else
				return "'" + s + "'";
		}

		protected String prepareDate(String s) {
			Date d = parseDate(s);
			String dateString = new SimpleDateFormat(DF_TO_ORACLE).format(d);
			return "to_date('" + dateString + "', 'YYYYMMDDHH24MISS')";
		}

		protected void setDescription(String description) {
			this.description = description;
		}

		public String getDescription() {
			return description;
		}
	}

	private static Date parseDate(String s) {
		for (String f : DF_INPUTS) {
			try {
				return new SimpleDateFormat(f).parse(s);
			} catch (ParseException e) {
			}
		}
		return null;
	}

	public enum Operator {
		EQ("=", "NE", "Equals"), //
		GT(">", "LT", "Greater Than"), //
		LT("<", "GT", "Lower Than"), //
		NE("<>", "EQ", "Not Equal"), //
		GE(">=", "GE", "Greater or Equal"), //
		LE("<=", "LE", "Lower or Equal"), //
		BT("between", "BT", "between") {
			public String asSqlString(Criterion c, Field field) {
				return " between " + field.prepareString(c.getLow()) + " and "
						+ field.prepareString(c.getHigh()) + " ";
			}
		};

		private String opositeOperator;

		private Operator(String s, String op, String description) {
			this.opString = s;
			this.description = description;
			this.opositeOperator = op;
		}

		public String createCondition(Criterion c) {
			StringBuilder b = new StringBuilder();
			Field f = Field.valueOf(c.getField().toUpperCase());
			b.append("\n    ").append(f.getFieldName())
					.append(f.getCondition(this, c));
			return b.toString();
		}

		private String opString, description;

		public String asSqlString(Criterion c, Field field) {
			return " " + opString + " " + field.prepareString(c.getLow()) + " ";
		}

		public String getDescription() {
			return description;
		}
	}

	private StringBuilder includeCond = new StringBuilder(),
			excludeCond = new StringBuilder();
	private String includeConj = "", excludeConj = "";

	public SearchStringBuilder append(Criterion c) {
		Operator operator = Operator.valueOf(c.getOperator().toUpperCase());
		if ("I".equalsIgnoreCase(c.getType())) {
			String part = operator.createCondition(c);
			includeCond.append(includeConj).append(part);
			includeConj = " OR ";
		} else if ("E".equalsIgnoreCase(c.getType())) {
			// operator = operator.oposite();
			Criterion negotiate = c.negotiate(operator.opositeOperator);
			String part = operator.createCondition(negotiate);
			excludeCond.append(excludeConj).append(part);
			excludeConj = " AND ";
		}
		return this;
	}

	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(stringBuilder.toString());
		if (excludeCond.length() > 0 || includeCond.length() > 0)
			b.append(" WHERE ");
		if (excludeCond.length() > 0) {
			b.append("(").append(excludeCond.toString()).append(")");
			if (includeCond.length() > 0)
				b.append("\n AND ");
		}
		if (includeCond.length() > 0)
			b.append("(").append(includeCond.toString()).append(")");
		b.append("\n   ORDER BY sendingDate");
		return b.toString();
	}

	public static String createSqlString(AbstractRequest request) {
		SearchStringBuilder ssb = new SearchStringBuilder();
		for (Criterion c : request.getCriterions()) {
			ssb.append(c);
		}
		return ssb.toString();
	}
}
