package de.lsv.pi.message.archive.db.save;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.db.PersistMessage;
import de.lsv.pi.message.archive.db.PersistMessageKey;
import de.lsv.pi.message.archive.db.query.SearchStringBuilder;
import de.lsv.pi.message.archive.extract.ExtractMessageLocal;
import de.lsv.pi.message.archive.query.SaveRequest;
import de.lsv.pi.message.archive.query.SaveResponse;
import de.lsv.pi.message.archive.save.SaveMessageLocal;

@Stateless
@Local(value = { SaveMessageLocal.class })
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class SaveMessageBean implements SaveMessageLocal {
	@PersistenceContext(unitName = "de.lsv.pi.message.archive.db.ejb")
	private EntityManager em;

	@EJB
	private ExtractMessageLocal extractor;

	private final static Location loc = Location.getLocation(SaveMessageBean.class);

	private class MessageCache {
		private Date sendingDate;
		private long most, least;
		private SaveFactory saveFactory;

		public SaveFactory getSaveFactory() {
			return saveFactory;
		}

		public MessageCache(SaveFactory saveFactory, PersistMessage p) {
			this.saveFactory = saveFactory;
			sendingDate = p.getSendingDate();
			most = p.getKey().getMost();
			least = p.getKey().getLeast();
		}


		public PersistMessage toPersistenceMessage() {
			PersistMessage tmp = new PersistMessage();
			tmp.setSendingDate(sendingDate);
			PersistMessageKey key = new PersistMessageKey(least, most);
			tmp.setKey(key);
			;
			return tmp;
		}

		public void persist() throws IOException {
			saveFactory.save(toPersistenceMessage());
		}
	}

	private class FileStatusLog implements StatusLog {

		private Path statusFile;
		private int amount;
		private int stepSize = 1000;
		private int size;

		public FileStatusLog(Path p) {
			this.statusFile = p;
		}

		@Override
		public void init(int size) {
			this.size = size;
		}

		@Override
		public synchronized void saved(String id) {
			this.amount++;
			if (this.amount == this.size || this.amount % this.stepSize == 0) {
				List<String> content = new ArrayList<>();
				content.add(String.format("%d of %d", this.amount, this.size));
				try {
					Files.write(this.statusFile, content, StandardOpenOption.CREATE);
				} catch (Exception e1) {
					loc.debugT("Error writing status file {0} \n {1} \n {2}",
							new Object[] { this.statusFile, e1, content });
				}
			}
		}
	}

	@Lock(LockType.WRITE)
	public SaveResponse saveMessages(SaveRequest req, StatusLog log) throws IOException {
		loc.entering("saveMessages", new Object[] { req });
		ExecutorService service = Executors.newSingleThreadExecutor();
		UUID uuid = UUID.randomUUID();
		service.submit(() -> {
			try {
				asyncRun(req,uuid);
			} catch (Exception e) {
				loc.catching(e);
			}
		});
		service.shutdown();
		return buildResponse(req,uuid);
	}

	private SaveResponse buildResponse(SaveRequest req, UUID uuid) {
		SaveResponse response = new SaveResponse();
		response.setMessages(new ArrayList<Message>());
		response.setProcessUUID(uuid.toString());
		response.setPath(Paths.get(req.getPathToSave(), uuid.toString()).toString());
		response.setLength(-1);
		return response;
	}

	private void asyncRun(SaveRequest req, UUID uuid) throws IOException {
		try {
			List<PersistMessage> sqlResult = executeSelect(req);
			if (req.getOffset() > -1 && req.getLength() > 0) {
				if (req.getOffset() < sqlResult.size()) {
					int toIndex = req.getOffset() + req.getLength();
					toIndex = Math.min(sqlResult.size(), toIndex);
					sqlResult = sqlResult.subList(req.getOffset(), toIndex);
				} else {
					return ;
				}
			}
			List<MessageCache> cachedMessages = applySaveFactory(req, sqlResult,uuid);
			loc.debugT("Cached Result {0}", new Object[] { cachedMessages.size() });
			if (cachedMessages.isEmpty() == false) {
				Path path = Paths.get(req.getPathToSave(), uuid.toString());
				FileStatusLog fsLog = new FileStatusLog(path);
				fsLog.init(cachedMessages.size());
				persistMessages(fsLog, cachedMessages,uuid);
			}
			loc.debugT("saveMessages was successfull");
		} finally {
			loc.exiting("saveMessages");
		}
	}

	private void persistMessages(StatusLog log, List<MessageCache> cachedMessages, UUID uuid) {
		cachedMessages.parallelStream().map(cm -> {
			try {
				PersistMessage persistenceMessage = cm.toPersistenceMessage();
				UUID asUUID = persistenceMessage.getKey().asUUID();
				loc.debugT("Start persist: {0}", new Object[] { asUUID });
				cm.persist();
				loc.debugT("End persist: {0}", new Object[] { asUUID });
				log.saved(asUUID.toString());
			} catch (Exception e) {
				loc.catching(e);
				Path statusPath = cm.getSaveFactory().baseDirectory().resolve("SaveMessagesError.txt");
				List<String> content = new ArrayList<>();
				content.add(uuid.toString());
				content.add(new Date().toString());
				content.add(String.format("written files amount %d", cachedMessages.size()));
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				e.printStackTrace(new PrintStream(bos));
				content.add(bos.toString());
				try {
					Files.write(statusPath, content, StandardOpenOption.CREATE_NEW);
				} catch (Exception e1) {
					loc.debugT("Error writing status file {0} \n {1} \n {2}", new Object[] { statusPath, e1, content });
				}

				throw new RuntimeException("Error during message persistence: ", e);
			}
			return cm;
		}).collect(Collectors.reducing((a, c) -> {
			return a != null ? a : c;
		})).ifPresent(cm -> {
			loc.debugT("Writing Status File for {0}", new Object[] { uuid });
			Path statusPath = cm.getSaveFactory().baseDirectory().resolve("SaveMessagesFinished.txt");
			List<String> content = new ArrayList<>();
			content.add(uuid.toString());
			content.add(new Date().toString());
			content.add(String.format("written files amount %d", cachedMessages.size()));
			try {
				Files.write(statusPath, content, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (Exception e) {
				loc.debugT("Error writing status file {0} \n {1} \n {2}", new Object[] { statusPath, e, content });
			}
		});
	}

	private List<MessageCache> applySaveFactory(SaveRequest req, List<PersistMessage> sqlResult, UUID uuid)
			throws IOException {
		loc.entering("applySaveFactory", new Object[] { req, uuid,sqlResult.size() });
		SaveFactoryProvider sfProvider = new SaveFactoryProvider(em, extractor, req.getPathToSave(), req.getOffset());
		List<MessageCache> cachedMessages = sqlResult.stream().map(pm -> {
			try {
				return new MessageCache(sfProvider.next(), pm);
			} catch (Exception e) {
				throw new RuntimeException("SaveFactoryProvider.next() failed", e);
			}
		}).collect(Collectors.toList());
		loc.exiting("applySaveFactory", cachedMessages.size());
		return cachedMessages;
	}

	private List<PersistMessage> executeSelect(SaveRequest req) {
		String b = SearchStringBuilder.createSqlString(req);
		loc.debugT(b);
		@SuppressWarnings("unchecked")
		List<PersistMessage> sqlResult = (List<PersistMessage>) em//
				.createNativeQuery(b, PersistMessage.class)//
				.getResultList();
		loc.debugT("Result {0} for {1}", new Object[] { sqlResult.size(), b });
		return sqlResult;
	}

}
