package de.lsv.pi.message.archive.db.save;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ListIterator;

import javax.persistence.EntityManager;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.db.PersistMessage;
import de.lsv.pi.message.archive.db.query.DimensionCache;
import de.lsv.pi.message.archive.db.util.PathProvider;
import de.lsv.pi.message.archive.extract.ExtractMessageLocal;
import de.lsv.pi.message.archive.io.ArchiveMessage;

public abstract class SaveFactory {
	private final static String FILE_NAME_TEMPLATE = "%s-%s-%02d.%s";
	private final static Location loc = Location.getLocation(SaveFactory.class);

	public static SaveFactory newInstance(EntityManager em,
			final ExtractMessageLocal extractor, PathProvider pathProvider, final String pathToSave)
			throws IOException {
		loc.entering();
		final DimensionCache cache = new DimensionCache(em);
		final File dir = new File(pathToSave);
		if (dir.exists() == false)
			dir.mkdirs();
		if (dir.exists() == false)
			throw new IOException("Cann't create directory: " + pathToSave);
		loc.exiting();
		return new SaveFactory() {

			@Override
			public Message save(PersistMessage next) throws IOException {
				loc.entering("save",new Object[]{next});
				SimpleDateFormat TIMESTAMP = new SimpleDateFormat(
						"yyyyMMddHHmmss");
				byte[] content = extractor.getMessageContent(next.getKey()
						.asUUID());
				ArchiveMessage msg = ArchiveMessage.read(content);
				String sendDate = TIMESTAMP.format(next.getSendingDate());
				String uid = next.getKey().asUUID().toString();
				String fileName = String.format(FILE_NAME_TEMPLATE, sendDate,
						"MainDocument", 0, uid);
				File file = new File(dir, fileName);
				write(file, msg.getMainDocument());

				for (ListIterator<AttachmentPart> i = msg.getAttachments(); i
						.hasNext();) {
					int idx = i.nextIndex();
					AttachmentPart attachment = i.next();
					fileName = String.format(FILE_NAME_TEMPLATE, sendDate, uid,
							"Attachment", idx);
					try {
						write(new File(dir, fileName), attachment
								.getRawContentBytes());
					} catch (SOAPException e) {
						throw new IOException(e);
					}
				}
				Message load = cache.load(next);
				loc.exiting("save",load);
				return load;
			}

			private void write(File file, byte[] mainDocument)
					throws IOException {
				loc.entering("write",new Object[]{file});
				FileOutputStream os = new FileOutputStream(file);
				try {
					if (file.getParentFile().exists() == false)
						file.getParentFile().mkdirs();
					os.write(mainDocument);
				} catch(IOException e){
					loc.catching("write",e);
					throw e;
				}finally {
					os.close();
					loc.exiting("write");
				}
			}

			@Override
			public Path baseDirectory() {
				return Paths.get(pathProvider.getBaseDir().toURI());
			}

		};
	}

	public abstract Message save(PersistMessage next) throws IOException;
	public abstract Path baseDirectory();
}
