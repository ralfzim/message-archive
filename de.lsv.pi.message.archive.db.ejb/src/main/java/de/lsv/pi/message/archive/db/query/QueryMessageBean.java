package de.lsv.pi.message.archive.db.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.db.PersistMessage;
import de.lsv.pi.message.archive.query.AbstractRequest;
import de.lsv.pi.message.archive.query.QueryMessageLocal;
import de.lsv.pi.message.archive.query.SearchException;
import de.lsv.pi.message.archive.query.SearchRequest;
import de.lsv.pi.message.archive.query.SearchResponse;

/**
 * Session Bean implementation class QueryMessageBean
 */
@Stateless
@Local({ QueryMessageLocal.class })
@TransactionAttribute()
public class QueryMessageBean implements QueryMessageLocal {
	static final String REAL_EMPTY = "";
	static final String DB_EMPTY = "\\";
	private final static Location loc = Location.getLocation(QueryMessageBean.class);

	/**
	 * Default constructor.
	 */
	public QueryMessageBean() {
	}

	@PersistenceContext(unitName = "de.lsv.pi.message.archive.db.ejb")
	private EntityManager em;

	/**
	 * @see QueryMessageLocal#search(Collection<Criterion>)
	 */
	@SuppressWarnings("unchecked")
	public SearchResponse search(SearchRequest request) {
		loc.entering("search", new Object[] { request });
		if (request.getLength() == 0)
			request.setLength(100);
		String b = SearchStringBuilder.createSqlString(request);
		loc.debugT("search", "SearchString: {0}", new Object[] { b });
		List<Message> result = new ArrayList<Message>();
		List<PersistMessage> sqlResult = (List<PersistMessage>) em//
				.createNativeQuery(b, PersistMessage.class)//
				.getResultList();
		int size = sqlResult.size();
		loc.debugT("search", "SearchResult {0} for {1}", new Object[] { size, b });
		if (request.getStart() >= size)
			sqlResult = Collections.emptyList();
		else {
			int a = request.getStart()+request.getLength();
			sqlResult = sqlResult.subList(request.getStart(), Math.min(a, size));
		}
		
		
		DimensionCache cache = new DimensionCache(em);
		sqlResult.forEach(is ->{
			result.add(cache.load(is));
		});
		SearchResponse response = new SearchResponse();
		response.setMessages(result);
		response.setStartRange(request.getStart());
		response.setEndRange(response.getStartRange() + result.size());
		response.setLength(size);
		loc.exiting(response.getLength());
		return response;
	}

	@SuppressWarnings("unchecked")
	public void delete(AbstractRequest searchReq) throws SearchException {
		loc.entering();
		String b = SearchStringBuilder.createSqlString(searchReq);
		loc.debugT(b);
		List<PersistMessage> msgs = em.createNativeQuery(b, PersistMessage.class).getResultList();
		for (Iterator<PersistMessage> i = msgs.iterator(); //
		i.hasNext(); //
		em.remove(i.next()))
			;
		loc.exiting();
	}

	public Collection<SearchEntity> listCategories() throws SearchException {
		Collection<SearchEntity> tmp = new ArrayList<SearchEntity>();
		for (SearchStringBuilder.Field f : SearchStringBuilder.Field.values()) {
			tmp.add(new SearchEntity(f.toString(), f.getDescription()));
		}
		return tmp;
	}

	public Collection<SearchEntity> listOperators() throws SearchException {
		Collection<SearchEntity> tmp = new ArrayList<SearchEntity>();
		for (SearchStringBuilder.Operator f : SearchStringBuilder.Operator.values()) {
			tmp.add(new SearchEntity(f.toString(), f.getDescription()));
		}
		return tmp;
	}

}
