package de.lsv.pi.message.archive.impl;

import java.util.UUID;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.PersistMessageBeanLocal;

/**
 * Session Bean implementation class PersistMessageBean
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class PersistMessageBean implements PersistMessageBeanLocal {

	private static final String SAVE = "save";
	private static final String DELETE = "deleteOnFault";
	private final static Location loc = Location
			.getLocation(PersistMessageBean.class);
	@PersistenceContext(unitName = "de.lsv.pi.message.archive.db.ejb")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public PersistMessageBean() {
	}

	public void reset(UUID m) {
		// PersistMessageKey pmk = createKey(m);
		// PersistMessage pm = em.find(PersistMessage.class, pmk);
		// if (pm != null)
		// em.remove(pm);

	}

	@Lock(LockType.WRITE)
	public void save(Message m) {
		loc.entering(SAVE,new Object[]{m});
		try {
			new Persist(em).save(m);
		} catch(Throwable e){
			loc.catching(e);
			loc.errorT(SAVE, "{0}\n{1}", Trace.toObject(m,e));
			throw new RuntimeException(e);
		}finally {
			loc.exiting(SAVE,m);
		}
	}

	//@Override
	public void deleteOnFault(Message sapMain) {
		loc.entering(DELETE);
		try {
			new Remove(em).save(sapMain);
		} catch(Throwable e){
			loc.catching(e);
			loc.errorT(DELETE, "em error: {0}", Trace.toObject(e));
			throw new RuntimeException(e);
		}finally {
			loc.exiting(DELETE);
		}
	}

}
