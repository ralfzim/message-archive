/**
 * 
 */
package de.lsv.pi.message.archive.impl;

import java.util.UUID;

import javax.persistence.EntityManager;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.db.DimensionAccess;
import de.lsv.pi.message.archive.db.PersistContentKey;
import de.lsv.pi.message.archive.db.PersistMessage;
import de.lsv.pi.message.archive.db.PersistMessageContent;
import de.lsv.pi.message.archive.db.PersistMessageKey;

class Persist {
	private static final String SAVE = "save";
	private final static Location loc = Location.getLocation(Persist.class);
	private EntityManager em;

	public Persist(EntityManager em2) {
		this.em = em2;
	}

	public void save(Message m) {
		loc.entering(SAVE,new Object[]{m});
		boolean update;
		PersistMessageKey key = createKey(m.getMessageId());
		PersistMessage pm = em.find(PersistMessage.class, key);
		if (pm != null) {
			update = true;
			loc.debugT(SAVE, "found allways message {0}", Trace.toObject(pm));
		} else {
			update = false;
			pm = new PersistMessage();
			pm.setKey(key);
		}
		for (DimensionAccess da : DimensionAccess.values()) {
			da.setDimension(m, pm, em);
		}
		pm.setDeletionDate(m.getDeletionDate());
		pm.setSendingDate(m.getSendingDate());
		if (update) {
			em.merge(pm);
			loc.debugT(SAVE, "merge msg {0}", Trace.toObject(pm));
		} else {
			em.persist(pm);
			loc.debugT(SAVE, "save msg {0}", Trace.toObject(pm));
		}
		persistContent(m, key);
//		em.flush();
		loc.exiting(SAVE,m);
	}

	private final static int RecordLength = 4000;

	private void persistContent(Message m, PersistMessageKey key) {
		loc.entering("persistContent",new Object[]{key,m});
		byte[] content = m.getContent();
		int l = 0;
		for (int offset = 0, maxLength = content.length; offset < maxLength; l++) {
			boolean update = false;
			PersistContentKey pKey = createContentKey(key, l);
			PersistMessageContent pc = em.find(PersistMessageContent.class,
					pKey);
			if (pc == null) {
				pc = new PersistMessageContent();
				pc.setKey(pKey);
			} else {
				update = true;
			}
			int length = Math.min(RecordLength, maxLength - offset);
			pc.setContent(new String(content, offset, length));
			loc.debugT("Merge/Create {0} ContentPart {1} of Message {2} with  {3}",new Object[]{update,l,key,m});
			if (update)
				em.merge(pc);
			else
				em.persist(pc);
			offset += length;
		}
		
		for (boolean toDelete = true;toDelete;l++) {
			PersistContentKey pKey = createContentKey(key, l);
			PersistMessageContent pc = em.find(PersistMessageContent.class,
					pKey);
			if (pc != null) {
				loc.debugT("Delete ContentPart {0} of Message {1} with  {2}",new Object[]{l,key,m});
				em.remove(pc);
			} else {
				toDelete = false;
			}
		}
		loc.exiting("persistContent",key);
	}

	private PersistContentKey createContentKey(PersistMessageKey key, int l) {
		PersistContentKey tmp = new PersistContentKey();
		tmp.setLeast(key.getLeast());
		tmp.setMost(key.getMost());
		tmp.setLinenr((short) l);
		return tmp;
	}

	private PersistMessageKey createKey(UUID messageId) {
		loc.entering("createKey");
		return new PersistMessageKey(messageId.getLeastSignificantBits(),
				messageId.getMostSignificantBits());
	}
}