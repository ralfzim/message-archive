package de.lsv.pi.message.archive.db;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersistMessageKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "LEAST")
	private long least;
	@Column(name = "MOST")
	private long most;

	public long getLeast() {
		return least;
	}

	public void setLeast(long least) {
		this.least = least;
	}

	public long getMost() {
		return most;
	}

	public void setMost(long most) {
		this.most = most;
	}

	public PersistMessageKey() {
		this(0L, 0L);
	}

	public PersistMessageKey(long least, long most) {
		super();
		this.least = least;
		this.most = most;
	}

	public UUID asUUID() {
		return new UUID(getMost(),getLeast());
	}
	
	@Override
	public String toString() {
		return "Key [least=" + least + ", most=" + most + "]";
	}

}
