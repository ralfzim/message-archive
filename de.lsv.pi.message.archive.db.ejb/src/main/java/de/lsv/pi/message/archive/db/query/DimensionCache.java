/**
 * 
 */
package de.lsv.pi.message.archive.db.query;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import de.lsv.pi.message.archive.Interface;
import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.Service;
import de.lsv.pi.message.archive.db.Dimension;
import de.lsv.pi.message.archive.db.PersistMessage;

public class DimensionCache {
	private Map<Integer, String> cache = new HashMap<Integer, String>();
	private EntityManager em;

	public DimensionCache(EntityManager em_) {
		this.em = em_;
	}

	public String getDimension(int id) {
		String value = cache.get(id);
		if (value == null) {
			value = getRawDimension(id);
			cache.put(id, value);
		}
		return value;
	}

	public Message load(PersistMessage next) {
		Message msg = new Message();
		msg.setMessageId(new UUID(next.getKey().getMost(),//
				next.getKey().getLeast()));
		msg.setSendingDate(next.getSendingDate());
		msg.setDeletionDate(next.getDeletionDate());
		Interface msgInterface = new Interface();
		msgInterface.setInterfaceName(this
				.getDimension(next.getInterfaceName()));
		msgInterface.setNamespaceURI(this.getDimension(next.getInterfaceURI()));
		msg.setMsgInterface(msgInterface);
		Service recServ = new Service();
		recServ.setName(this.getDimension(next.getReceiverServiceName()));
		recServ.setParty(this.getDimension(next.getReceiverServiceParty()));
		msg.setReceiverService(recServ);
		Service sendServ = new Service();
		sendServ.setName(this.getDimension(next.getSenderServiceName()));
		sendServ.setParty(this.getDimension(next.getSenderServiceParty()));
		msg.setSenderService(sendServ);
		return msg;
	}

	private String getRawDimension(int id) {
		Dimension dimension = em.find(Dimension.class, id);
		if (dimension == null)
			return QueryMessageBean.REAL_EMPTY;
		String value = dimension.getValue();
		value = QueryMessageBean.DB_EMPTY.equals(value) ? QueryMessageBean.REAL_EMPTY
				: value;
		return value;
	}

}