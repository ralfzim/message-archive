package de.lsv.pi.message.archive.db.save;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.save.SaveMessageStatus;
import de.lsv.pi.message.archive.save.SaveMessageStatusRequest;
import de.lsv.pi.message.archive.save.SaveMessageStatusResponse;
import de.lsv.pi.message.archive.save.SaveMessageStatusResponse.EvaluationState;

@Stateless
@Local(value = { SaveMessageStatus.class })
public class SaveMessageStatusBean implements SaveMessageStatus{
	private final static Location loc = Location.getLocation(SaveMessageStatusBean.class);
	@Override
	public SaveMessageStatusResponse updateStatus(SaveMessageStatusRequest req) {
		loc.entering("updateStatus",new Object[]{req});
		SaveMessageStatusResponse doUpdateStatus = doUpdateStatus(req);
		loc.exiting("updateStatus",doUpdateStatus);
		return doUpdateStatus;
	}
	private SaveMessageStatusResponse doUpdateStatus(SaveMessageStatusRequest req) {
		Path statusFile = Paths.get(req.getPath());
		if (Files.exists(statusFile) == false)
			return newStatus(EvaluationState.NoFile,req);
		try {
			List<String> lines = Files.readAllLines(statusFile);
			if (lines.size() != 1)
				return newStatus(EvaluationState.WrongFileContent, req);
			String[] parts = lines.get(0).split(" of ");
			if (parts.length != 2)
				return newStatus(EvaluationState.WrongFileContent,req);
			SaveMessageStatusResponse tmp = newStatus(EvaluationState.OK,req);
			tmp.setActualAmount(Integer.parseInt(parts[0]));
			tmp.setMaxAmount(Integer.parseInt(parts[1]));
			return tmp ;
		} catch (IOException e) {
			return newStatus(EvaluationState.IoException,req);
		} catch (NumberFormatException e) {
			return newStatus(EvaluationState.NumberFormat,req);
		}
	}
	private SaveMessageStatusResponse newStatus(EvaluationState nofile, SaveMessageStatusRequest req) {
		SaveMessageStatusResponse tmp = new SaveMessageStatusResponse();
		tmp.setEvaluationState(nofile);
		tmp.setPath(req.getPath());
		tmp.setProcessID(req.getProcessID());
		return tmp;
	}

}
