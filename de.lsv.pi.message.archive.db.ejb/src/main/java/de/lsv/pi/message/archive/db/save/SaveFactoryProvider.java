package de.lsv.pi.message.archive.db.save;

import java.io.File;
import java.io.IOException;

import javax.persistence.EntityManager;

import de.lsv.pi.message.archive.db.util.PathProvider;
import de.lsv.pi.message.archive.extract.ExtractMessageLocal;

public class SaveFactoryProvider {
	
	
	private EntityManager entityManager;
	private ExtractMessageLocal extractor;
	private SaveFactory next;
	private PathProvider pathProvider;
	public SaveFactoryProvider(EntityManager em, ExtractMessageLocal extractor, PathProvider pathProvider) throws IOException {
		this.entityManager = em;
		this.extractor = extractor;
		this.pathProvider = pathProvider;
		next = SaveFactory.newInstance(entityManager, extractor, pathProvider,this.pathProvider.getPathAsString());
	}

	
	
	public SaveFactoryProvider(EntityManager em, ExtractMessageLocal extractor2, String pathToSave) throws IOException {
		this(em, extractor2, new PathProvider(new File(pathToSave)));
	}

	public SaveFactoryProvider(EntityManager em, ExtractMessageLocal extractor2, String pathToSave,long offset) throws IOException {
		this(em, extractor2, new PathProvider(new File(pathToSave),offset));
	}


	public SaveFactory next() throws IOException {

		if (pathProvider.hasNewPath()) {
			next = SaveFactory.newInstance(entityManager, extractor,pathProvider, pathProvider.getPathAsString());
		}
		return next;
	}
}