package de.lsv.pi.message.archive.db.job;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.sap.scheduler.runtime.JobContext;
import com.sap.scheduler.runtime.JobDefinitionID;
import com.sap.scheduler.runtime.JobParameter;
import com.sap.scheduler.runtime.JobParameterDefinition;
import com.sap.scheduler.runtime.NoSuchJobDefinitionException;
import com.sap.scheduler.runtime.ParameterValidationException;
import com.sap.scheduler.runtime.mdb.MDBJobImplementation;

import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.extract.ExtractMessageLocal;
import de.lsv.pi.message.archive.io.ArchiveMessage;
import de.lsv.pi.message.archive.query.QueryMessageLocal;
import de.lsv.pi.message.archive.query.SearchException;
import de.lsv.pi.message.archive.query.SearchRequest;
import de.lsv.pi.message.archive.query.SearchResponse;

/**
 * Message-Driven Bean implementation class for: SchedulerJob
 *
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "JobDefinition=\'UpdateSendingDateJob2\'") })
public class UpdateSendingDateJob2Bean extends MDBJobImplementation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see MDBJobImplementation#MDBJobImplementation()
	 */
	public UpdateSendingDateJob2Bean() {
		super();
	}

	@EJB
	private QueryMessageLocal queryBean;
	@EJB
	private ExtractMessageLocal extractBean;
	@PersistenceContext(unitName = "de.lsv.pi.message.archive.db.ejb")
	private EntityManager em;

	@Override
	public void onJob(final JobContext jobContext) throws Exception {
		JobParameter offsetParameter = jobContext.getJobParameter("Offset");
		Integer offsetValue = offsetParameter.getIntegerValue();
		int offset = offsetValue != null ? offsetValue.intValue() : -1;
		if (offset == -1)
			createSubJobs(jobContext);
		else 
			executeUpdate(jobContext,offset);

	}

	private void executeUpdate(JobContext jobContext, int offset) throws SearchException, IOException {
		Logger logger = jobContext.getLogger();
		SearchRequest request = new SearchRequest();
		request.setLength(100);
		request.setStart(offset);
		SearchResponse result = queryBean.search(request);
		for (Message m : result.getMessages()) {
			logger.info(String.format("update for message %s start", m
					.getMessageId().toString()));
			byte[] msgCnt = extractBean.getMessageContent(m.getMessageId());
			ArchiveMessage msg = ArchiveMessage.read(msgCnt);
			m = msg.getSapMain();
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(m.getSendingDate());
			cal.add(Calendar.YEAR, 10);
			m.setDeletionDate(cal.getTime());
			new Persist(em).save(m);
			logger.info(String.format("update for message %s done", m
					.getMessageId().toString()));
		}
	}

	private void createSubJobs(final JobContext jobContext)
			throws SearchException, ParameterValidationException,
			NoSuchJobDefinitionException {
		SearchRequest request = new SearchRequest();
		request.setLength(1);
		request.setStart(0);
		SearchResponse response = queryBean.search(request);
		int partLength = 100;
		JobDefinitionID id = jobContext.getJob().getJobDefinitionId();

		JobParameterDefinition def = jobContext.getJobParameterDefinition(
				"UpdateSendingDateJob2", "Offset");

		for (int startPos = 0, length = response.getLength(); startPos < length; startPos += partLength) {
			jobContext.getLogger().info(
					String.format("Start processing on %d of %d", startPos,
							length));
			JobParameter[] params = { new JobParameter(def, startPos) };
			jobContext.executeJob(id, params);
		}
	}


}
