package de.lsv.pi.message.archive.db.extract;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.db.PersistMessageContent;
import de.lsv.pi.message.archive.db.PersistMessageKey;
import de.lsv.pi.message.archive.extract.ExtractMessageLocal;
import de.lsv.pi.message.archive.io.IOUtils;
import de.lsv.pi.message.archive.io.MessageInputStream;


@Stateless
public class ExtractMessageBean implements ExtractMessageLocal {

	private static final String GET_MESSAGE_CONTENT = "getMessageContent";
	private final static Location loc = Location
			.getLocation(ExtractMessageBean.class);
	@PersistenceContext(unitName = "de.lsv.pi.message.archive.db.ejb")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public byte[] getMessageContent(UUID uid) throws IOException {
		loc.entering(GET_MESSAGE_CONTENT);
		try {
			loc.debugT(GET_MESSAGE_CONTENT, "uid is {0}", new Object[] { uid });
			loc.debugT(GET_MESSAGE_CONTENT, "uid is {0}", new Object[] { em.getDelegate() });
			
			PersistMessageKey key = new PersistMessageKey();
			key.setLeast(uid.getLeastSignificantBits());
			key.setMost(uid.getMostSignificantBits());
			
			Query query = em.createNamedQuery("PersistMessageContent.findByKey");
			query.setParameter("most", key.getMost());
			query.setParameter("least", key.getLeast());
			StringBuilder b = new StringBuilder();
			for (PersistMessageContent c : (List<PersistMessageContent>)query.getResultList()) {
				b.append(c.getContent());
			}
			String content = b.toString();
			loc.debugT(GET_MESSAGE_CONTENT, "content length {0}", new Object[] {content.length()});
			MessageInputStream is = new MessageInputStream(content.getBytes());
			byte[] result = IOUtils.toByteArray(is);
			loc.debugT(GET_MESSAGE_CONTENT, "result length {0} >> {1}", new Object[] {result.length, result });
			return result;
		} finally {
			loc.exiting(GET_MESSAGE_CONTENT);
		}
	}

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

}
