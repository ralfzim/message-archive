package de.lsv.pi.message.archive.db.job;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sap.scheduler.runtime.JobContext;
import com.sap.scheduler.runtime.JobParameter;
import com.sap.scheduler.runtime.JobParameterDefinition;

import de.lsv.pi.message.archive.db.util.PathProvider;
import de.lsv.pi.message.archive.query.Criterion;

public class MessageArchiveParameters {

	public enum Parameter {
		Offset, 
//		ParallelJobs,
		ChunkSize,
//		JobOffset,
		ArchivePath, Year, Month;
		int evalIntValue(JobContext jobContext, int defValue) {
			JobParameter param = jobContext.getJobParameter(toString());
			Integer intValue = param.getIntegerValue();
			int i = intValue != null ? intValue.intValue() : defValue;
			return i == -1 ? defValue : i;
		}

		String evalStringValue(JobContext jobContext, String defValue) {
			JobParameter param = jobContext.getJobParameter(toString());
			String strValue = param.getStringValue();
			return strValue != null ? strValue : defValue;
		}

		JobParameterDefinition getJobParameterDefinition(JobContext jobContext) {
			JobParameterDefinition def = jobContext.getJobParameterDefinition(
					"MessageArchive2FileJob", toString());
			return def;
		}

	}

	private int offset;
	private File archivePath;
	private int year;
	private int month;
	private JobContext jobContext;
	private File parentPath;
	private int parallelJobs;
	private int chunkSize;
	private int jobOffset;

	public MessageArchiveParameters(JobContext jobContext) {
		this.jobContext = jobContext;
		this.offset = Parameter.Offset.evalIntValue(jobContext, -1);
		this.archivePath = new File(Parameter.ArchivePath.evalStringValue(
				jobContext, "/tmp").trim());
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		this.year = Parameter.Year.evalIntValue(jobContext,
				cal.get(Calendar.YEAR));
		this.month = Parameter.Month.evalIntValue(jobContext,
				cal.get(Calendar.MONTH)+1)-1;
//		this.parallelJobs = Parameter.ParallelJobs.evalIntValue(jobContext, 5);
		this.chunkSize = Parameter.ChunkSize.evalIntValue(jobContext, 100000);
//		this.jobOffset = Parameter.JobOffset.evalIntValue(jobContext, 0);
	}

	public JobContext getJobContext() {
		return jobContext;
	}

	public int getOffset() {
		return offset;
	}

	public File getArchivePath() {
		return archivePath;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public File getParentPath() {
		baseDirectory();
		return parentPath;
	}

	public int getParallelJobs() {
		return parallelJobs;
	}

	public int getChunkSize() {
		return chunkSize;
	}

	public int getJobOffset() {
		return jobOffset;
	}

	public Criterion[] getCriterions() {
		GregorianCalendar cal = new GregorianCalendar(this.year, this.month, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String low = sdf.format(cal.getTime());
		cal.add(Calendar.MONTH, 1);
		String high = sdf.format(cal.getTime());
		return new Criterion[] { new Criterion().setField("DTS").setType("E")
				.setOperator("BT").setLow(low).setHigh(high) };
	}

	public JobParameter[] getSubJobParameters(int startPos) {
		return new JobParameter[]{
				new JobParameter(Parameter.ArchivePath.getJobParameterDefinition(jobContext), this.archivePath.toString()), //
				new JobParameter(Parameter.Year.getJobParameterDefinition(jobContext), this.year), //
				new JobParameter(Parameter.Month.getJobParameterDefinition(jobContext), this.month+1), //
				new JobParameter(Parameter.Offset.getJobParameterDefinition(jobContext), startPos), //
//				new JobParameter(Parameter.ParallelJobs.getJobParameterDefinition(jobContext), this.parallelJobs), //
				new JobParameter(Parameter.ChunkSize.getJobParameterDefinition(jobContext), this.chunkSize), //
//				new JobParameter(Parameter.JobOffset.getJobParameterDefinition(jobContext), this.jobOffset), //
		};
	}


	public void setJobOffset(int jobOffset) {
		this.jobOffset = jobOffset;
	}

	private void baseDirectory() {
		if (this.parentPath == null) {
			this.parentPath = this.archivePath;
			this.parentPath = new File(this.parentPath,Integer.toString(this.year));
			this.parentPath = new File(this.parentPath,Integer.toString(this.month+1));
			if (this.parentPath.exists() == false)
				this.parentPath.mkdirs();
		}
	}

	public PathProvider getPathProvider() {
		baseDirectory();
		return new PathProvider(parentPath, getStartPos());
	}

	public int getStartPos() {
		return offset*parallelJobs*chunkSize+jobOffset*chunkSize;
	}
	
}
