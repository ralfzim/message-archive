package de.lsv.pi.message.archive.db.job;

import java.io.IOException;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;

import com.sap.scheduler.runtime.JobContext;
import com.sap.scheduler.runtime.JobDefinitionID;
import com.sap.scheduler.runtime.JobParameter;
import com.sap.scheduler.runtime.NoSuchJobDefinitionException;
import com.sap.scheduler.runtime.ParameterValidationException;
import com.sap.scheduler.runtime.SchedulerRuntimeException;
import com.sap.scheduler.runtime.mdb.MDBJobImplementation;

import de.lsv.pi.message.archive.query.SaveRequest;
import de.lsv.pi.message.archive.query.SaveResponse;
import de.lsv.pi.message.archive.query.SearchRequest;
import de.lsv.pi.message.archive.save.SaveMessageLocal;
import de.lsv.pi.message.archive.save.SaveMessageLocal.StatusLog;

/**
 * Message-Driven Bean implementation class for: SchedulerJob
 *
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "JobDefinition=\'MessageArchive2FileJob\'") })
public class MessageArchive2FileJobBean extends MDBJobImplementation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see MDBJobImplementation#MDBJobImplementation()
	 */
	public MessageArchive2FileJobBean() {
		super();
	}

	@EJB
	private SaveMessageLocal saveBean;

	@Override
	public void onJob(final JobContext jobContext) throws Exception {
		MessageArchiveParameters maps = new MessageArchiveParameters(jobContext);
		if (executeUpdateDo(maps))
			createSubJob(maps, maps.getOffset() + maps.getChunkSize());
	
	}

	private boolean executeUpdateDo(MessageArchiveParameters maps)
			throws ParameterValidationException, NoSuchJobDefinitionException, SchedulerRuntimeException {
		try {
			return executeUpdate(maps);
		} catch (IOException e) {
			maps.getJobContext().getLogger().info(e.getMessage());
			createSubJob(maps, maps.getOffset());
			return false;
		}
	}

	private boolean executeUpdate(MessageArchiveParameters maps) throws IOException {
		SaveRequest request = new SaveRequest();
		request.setCriterions(maps.getCriterions());
		request.setOffset(maps.getOffset());
		request.setLength(maps.getChunkSize());
		request.setPathToSave(maps.getParentPath().toString());
		StatusLog log = getStatusLog(maps);
		SaveResponse response = saveBean.saveMessages(request, log);
		return response.getLength() == maps.getChunkSize();
	}

	void createSubJob(MessageArchiveParameters maps, int offset)
			throws ParameterValidationException, NoSuchJobDefinitionException, SchedulerRuntimeException {
		JobContext jobContext = maps.getJobContext();
		SearchRequest request = new SearchRequest();
		request.setCriterions(maps.getCriterions());
		// request.setLength(100);
		int startPos = offset;
		request.setStart(startPos);
		JobParameter[] params = maps.getSubJobParameters(startPos);
		JobDefinitionID id = jobContext.getJob().getJobDefinitionId();
		jobContext.executeJob(id, params);
		String msg = String.format("create next job on %d length %d", startPos, 100);
		jobContext.getLogger().info(msg);
	}

	private StatusLog getStatusLog(MessageArchiveParameters maps) {
		return new StatusLog() {
			private int amount = maps.getOffset();
			private JobContext jobContext = maps.getJobContext();

			@Override
			public void saved(String id) {
				amount++;
				if (amount % 1000 == 0)
					jobContext.getLogger().info(String.format("Writing %d", amount));
			}

			@Override
			public void init(int size) {

			}
		};
	}

}
