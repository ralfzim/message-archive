package de.lsv.pi.message.archive.db;

import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.ArchiveConstants;
import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.impl.Trace;

public enum DimensionAccess implements ArchiveConstants {

	senderServiceParty(DMT_senderServiceParty) {
		@Override
		public String getValue(Message m) {
			return m.getSenderService() != null ? m.getSenderService()
					.getParty() : null;
		}

		@Override
		public void setDimension(PersistMessage p, Dimension dimension) {
			p.setSenderServiceParty(dimension.getId());
		}
	},

	receiverServiceParty(DMT_receiverServiceParty) {
		@Override
		public String getValue(Message m) {
			return m.getReceiverService() != null ? m.getReceiverService()
					.getParty() : null;
		}

		@Override
		public void setDimension(PersistMessage p, Dimension dimension) {
			p.setReceiverServiceParty(dimension.getId());
		}
	},

	senderServiceName(DMT_senderServiceName) {
		@Override
		public String getValue(Message m) {
			return m.getSenderService() != null ? m.getSenderService()
					.getName() : null;
		}

		@Override
		public void setDimension(PersistMessage p, Dimension dimension) {
			p.setSenderServiceName(dimension.getId());
		}
	},

	receiverServiceName(DMT_receiverServiceName) {
		@Override
		public String getValue(Message m) {
			return m.getReceiverService() != null ? m.getReceiverService()
					.getName() : null;
		}

		@Override
		public void setDimension(PersistMessage p, Dimension dimension) {
			p.setReceiverServiceName(dimension.getId());
		}
	},

	senderInterfaceURI(DMT_senderInterfaceURI) {
		@Override
		public String getValue(Message m) {
			return m.getMsgInterface() != null ? m.getMsgInterface()
					.getNamespaceURI() : null;
		}

		@Override
		public void setDimension(PersistMessage p, Dimension dimension) {
			p.setInterfaceURI(dimension.getId());
		}
	},

	// receiverInterfaceURI(DMT_receiverInterfaceURI) {
	// @Override
	// public String getValue(Message m) {
	// return m.getReceiverInterface() != null ? m.getReceiverInterface()
	// .getNamespaceURI() : null;
	// }
	//
	// @Override
	// public void setDimension(PersistMessage p, Dimension dimension) {
	// p.setReceiverInterfaceURI(dimension);
	// }
	// },

	senderInterfaceName(DMT_senderInterfaceName) {
		@Override
		public String getValue(Message m) {
			return m.getMsgInterface() != null ? m.getMsgInterface()
					.getInterfaceName() : null;
		}

		@Override
		public void setDimension(PersistMessage p, Dimension dimension) {
			p.setInterfaceName(dimension.getId());
		}
	},

	// receiverInterfaceName(DMT_receiverInterfaceName) {
	// @Override
	// public String getValue(Message m) {
	// return m.getReceiverInterface() != null ? m.getReceiverInterface()
	// .getInterfaceName() : null;
	// }
	//
	// @Override
	// public void setDimension(PersistMessage p, Dimension dimension) {
	// p.setReceiverInterfaceName(dimension);
	// }
	// },

	;
	private static final String CREATE_NEW_DIMENSION = "createNewDimension";
	private static final String FIND_DIMENSION = "findDimension";
	private String type;
	private final static Location loc = Location
			.getLocation(DimensionAccess.class);

	private DimensionAccess(String type) {
		this.type = type;
	}

	public void setDimension(Message m, PersistMessage p, EntityManager em) {
		String dimensionValue = getValue(m);
		dimensionValue = dimensionValue != null && dimensionValue.trim().length() > 0 //
		? dimensionValue : "\\";
		Dimension dimension = findDimension(em, dimensionValue);
		setDimension(p, dimension);
	}

	public abstract void setDimension(PersistMessage p, Dimension dimension);

	@SuppressWarnings("unchecked")
	protected Dimension findDimension(EntityManager em, String dimensionValue) {
		loc.entering(FIND_DIMENSION);
		try {
			loc.debugT(FIND_DIMENSION, "search dimension {0}<{1}>",//
					Trace.toObject(type, dimensionValue));
			Query query = em.createNamedQuery(NQ_DIM_FindByTypeAndValue);
			query.setParameter("type", type);
			query.setParameter("value", dimensionValue);
			List<Dimension> resultList = query.getResultList();
			loc.debugT(FIND_DIMENSION, "result list size {0}",//
					Trace.toObject(resultList.size()));
			switch (resultList.size()) {
			case 0:
				return createNewDimension(em, dimensionValue);
			case 1:
				return resultList.get(0);
			default:
				loc.errorT(FIND_DIMENSION, "result list is larger than 1: {0} for dimension {1} / {2}",//
						Trace.toObject(resultList.size(),dimensionValue,type));
				for (Dimension d : resultList) {
					loc.errorT(FIND_DIMENSION,"SearchResult: {0}",Trace.toObject(d));
				}
//				return new ResolveDimension(em,resultList).process();
				return resultList.get(0);
			}
		} finally {
			loc.exiting(FIND_DIMENSION);
		}
	}

	private Dimension createNewDimension(EntityManager em, String dimensionValue) {
		loc.entering(CREATE_NEW_DIMENSION);
		loc.debugT(CREATE_NEW_DIMENSION, "create new dimension {0}<{1}>",//
				Trace.toObject(type, dimensionValue));
		Dimension dimension = new Dimension();
		dimension.setType(type);
		dimension.setValue(dimensionValue);
		Dimension dm;
		Random r = new Random(new Date().getTime());
		do {
			dimension.setId(r.nextInt());
			loc.debugT(CREATE_NEW_DIMENSION,
					"search dimension id {0} with {1}",//
					Trace.toObject(dimension, dimension.hashCode()));
			dm = em.find(Dimension.class, dimension.hashCode());
			loc.debugT(CREATE_NEW_DIMENSION, " found dimension id {0}",//
					Trace.toObject(dm));
		} while (dm != null);
		dimension.setId(dimension.hashCode());
		em.persist(dimension);
		em.flush();
		loc.debugT(CREATE_NEW_DIMENSION, "dimension saved {0}",//
				Trace.toObject(dimension));
		return dimension;
	}

	protected abstract String getValue(Message m);
}
