package de.lsv.pi.message.archive.db.query;

import static org.junit.Assert.*;

import org.junit.Test;

import de.lsv.pi.message.archive.query.Criterion;

public class SearchStringBuilderTest {

	@Test
	public void testAppend() {
		SearchStringBuilder ssb = new SearchStringBuilder();
		Criterion c = new Criterion().setLow("TEST_ANY").setField("ITN").setOperator("EQ").setType("I");
		ssb.append(c);
		String test = ssb.toString();
//		System.out.println(test);
		int r = test.indexOf("d.type = 'ITN' and d.value  = 'TEST_ANY'");
		assertTrue(test,r > -1);
	}
	
	@Test
	public void testTime() {
		SearchStringBuilder ssb = new SearchStringBuilder();
		Criterion c = new Criterion().setLow("20141012").setField("DTS").setOperator("GT").setType("I");
		ssb.append(c);
		String test = ssb.toString();
		int r = test.indexOf("SENDINGDATE > to_date('20141012000000', 'YYYYMMDDHH24MISS')");
		assertTrue(test,r > -1);
	}
	
	@Test
	public void testKey() {
		SearchStringBuilder ssb = new SearchStringBuilder();
		Criterion c = new Criterion().setLow("7479c0be-eb1a-11e3-a24f-000000bd25af").setField("KEY").setOperator("EQ").setType("I");
		ssb.append(c);
		String test = ssb.toString();
		int r = test.indexOf("MOST = 8392951306802303459 AND LEAST = -6751177316392688209");
		assertTrue(test,r > -1);
	}
	
	@Test
	public void testComplex() {
		SearchStringBuilder ssb = new SearchStringBuilder();
		ssb.append(new Criterion().setLow("20141012").setField("DTS").setOperator("GT").setType("I"));
		ssb.append(new Criterion().setLow("TEST_ANY").setField("ITN").setOperator("EQ").setType("I"));
		ssb.append(new Criterion().setLow("http://sap").setField("ITU").setOperator("LT").setType("E"));
		String text = ssb.toString();
		System.out.println(text);
	}

}
