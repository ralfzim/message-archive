package de.lsv.pi.message.archive.db.save;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.junit.Test;

import de.lsv.pi.message.archive.db.util.PathProvider;

public class PathProviderTest {

	@Test
	public void testPathProvider() {
		assertNotNull(new PathProvider(new File("/base")));
	}

	@Test
	public void testHasNewPath() {
		PathProvider path = new PathProvider(new File("/base"));
		for (int i = 1; i < 100000; i++)
			assertEquals(i%100==0, path.hasNewPath() );
	}

	@Test
	public void testGetPathAsString() {
		String arry[] = {
				"\\base\\00\\00\\00\\00", //
				"\\base\\00\\00\\00\\01" //
		};
		PathProvider path = new PathProvider(new File("/base/"));
		for (int i = 0; i < 200; i++) {
			assertEquals(arry[i/100], path.getPathAsString()); 
			path.hasNewPath();
		}
		
	}

}
