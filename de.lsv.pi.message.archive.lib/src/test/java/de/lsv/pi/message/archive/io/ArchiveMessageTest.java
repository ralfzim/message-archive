package de.lsv.pi.message.archive.io;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import de.lsv.pi.message.archive.Message;

public class ArchiveMessageTest {

	@Test
	public void testRead() throws IOException {
		InputStream is = ArchiveMessageTest.class.getResourceAsStream("ArchiveMessage1.txt");
		byte[] bytes = IOUtils.toByteArray(is);
		ArchiveMessage msg = ArchiveMessage.read(bytes);
		Message m = msg.getSapMain();
		System.out.println(m);
	}

}
