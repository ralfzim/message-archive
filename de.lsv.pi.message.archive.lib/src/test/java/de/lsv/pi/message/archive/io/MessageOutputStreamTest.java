package de.lsv.pi.message.archive.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ListIterator;

import junit.framework.TestCase;

public class MessageOutputStreamTest extends TestCase {

	private final static String en01 = "1f8b0800000000000000c5545b6fda30147e47ea3fc07294e785d83190cb4aa4b"
			+ "4a46b56085d0b6ceb0b32896933e5826283ca7efd9c40a0973d6c7b5984c4c9c977b1cf39b6a655cf60714b4bb"
			+ "130c9c240c4b02dab671a1ddc45d8b40941b847acb3d665910b960b6dba5b334711ec59e8cf59fa51899e68c999"
			+ "186cc44a93b0b3d6f9fdc4bbd5fc70eef8f996a5c59a29129873a7c90fd42721d68eaef3e889659477e4675ed07"
			+ "5a7281ff52ad0d981a8abee49ed9ad19895ee39a76b674c93fc85eadf281e6852e5c492c8a8c8f4e744ff16e863"
			+ "c6397d643a41aa52893bd9868b592ebdb9a0793c50b1aa6ce54b52e463faa328072a392592bc4a20f5b0ccbdd46"
			+ "54a3977bdf53a4d222a2ad83e7faebf03d5b4dbb2886432c91fc745cc5cca7779f4541679b1e17bca1bc04baf20"
			+ "764d142f9718638d2d11d3306644b34877a5a1fa59c6468fae5e594b4e2d314d32762f7bec1a087735d4d710996"
			+ "2ec20ec74d1c39e7184d404191d5b524dd04e916a79b4fb7d6555a56e0f1ba8df821aadea8d4cb94d22e65e0523"
			+ "7fe187d3afc1e5cd68167eda5b365f9bb793e51d8b58b2fdfd024e666f5cf483d06b722087bb5cd1882939cd185"
			+ "fcbe8b889946f3b31d3a78c0b2fdfa9eed4bf9f2ebcf0fb5ee7c83ce856a3d92c2f4de83265fb2acb66fdc789dd"
			+ "afe8cb86a689d84d564d4dfd671a897437c9a3c330be4334c57ab315997f77308f898b22dee92f11cd45e09eb5b"
			+ "47fbc70e8e9f4e8452498d0b82819cd4eb860e82855f18745b4c964a2ba8b66a107c1074569cbe802ccc2c92524"
			+ "e0666e12380218597d6459068237400e7cf5eb432c271f7826f1aef1d09b8ffc8a785d33941a6fe3ae056e2ee6f"
			+ "e550831c43de83db45be38907fcd95dbb1584236012cbc6a63c68d5eb158060029184b65b77e1507ae2fe070421"
			+ "ac1c313231814dd06e0d030f8c4dbb836df80982cf8621ffdaad913f04d036ed9e2d978a0122f264df1d5952190"
			+ "20b212245ab901808e13aacece7d2102113e03ec6c4207d6cd9f0b069705da43f3396b0127a792c2bc921967d40"
			+ "a681aa3d4f81fd66d755f601e0d7c5f9f37e6ada2f5a4cdd556b060000";

	public void testWriteByteArray() throws IOException {
		MessageOutputStream mos = new MessageOutputStream();

		mos.write("Not yet implemented".getBytes());
		mos.close();
		System.out.println(mos.toString());
		MessageInputStream mis = new MessageInputStream(mos.toByteArray());
		String txt = IOUtils.toString(mis);
		System.out.println(txt);
	}

	public void testReadByteArray() throws IOException {
		MessageInputStream mis = new MessageInputStream(en01.getBytes());
		System.out.println(IOUtils.toString(mis));
	}
	public void testParseByteArray() throws IOException {
		MessageInputStream mis = new MessageInputStream(en01.getBytes());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copyWithClose(mis, bos);
		ArchiveMessage msg = ArchiveMessage.read(bos.toByteArray());
		assertNotNull(msg);
		String msgContent = new String(msg.getMainDocument());
		assertTrue(msgContent.startsWith("UNA"));
		assertTrue(msgContent.contains("UNZ1A73AH1DAVLE"));
		assertTrue(msg.getAttachments() instanceof ListIterator<?>);
	}

}
