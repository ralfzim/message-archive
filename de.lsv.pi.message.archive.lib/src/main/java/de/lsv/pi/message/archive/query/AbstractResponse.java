package de.lsv.pi.message.archive.query;

import java.util.List;

import de.lsv.pi.message.archive.Message;

abstract class AbstractResponse {

	private Message[] messages;
	private int length;

	public Message[] getMessages() {
		return messages;
	}

	public void setMessages(Message[] messages) {
		this.messages = messages;
	}

	public void setMessages(List<Message> result) {
		this.messages = result.toArray(new Message[result.size()]);
	}

	public void setLength(int maxRows) {
		this.length = maxRows;
	}

	public int getLength() {
		return length;
	}

}
