package de.lsv.pi.message.archive;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Interface msgInterface;
	private Service senderService;
	private Service receiverService;
	private UUID messageId;
	private Date sendingDate;
	private Date deletionDate;
	private byte[] content;

	public Interface getMsgInterface() {
		return msgInterface;
	}

	public void setMsgInterface(Interface msgInterface) {
		this.msgInterface = msgInterface;
	}

	public Service getSenderService() {
		return senderService;
	}

	public void setSenderService(Service senderService) {
		this.senderService = senderService;
	}

	public Service getReceiverService() {
		return receiverService;
	}

	public void setReceiverService(Service receiverService) {
		this.receiverService = receiverService;
	}

	public UUID getMessageId() {
		return messageId;
	}

	public void setMessageId(UUID messageId) {
		this.messageId = messageId;
	}

	public Date getSendingDate() {
		return sendingDate;
	}

	public void setSendingDate(Date sendingDate) {
		this.sendingDate = sendingDate;
	}

	public Date getDeletionDate() {
		return deletionDate;
	}

	public void setDeletionDate(Date deletionDate) {
		this.deletionDate = deletionDate;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Message cloneMsg() {
		Message tmp = new Message();
		tmp.msgInterface = this.msgInterface;
		tmp.senderService = this.senderService;
		tmp.receiverService = this.receiverService;
		tmp.messageId = this.messageId;
		tmp.sendingDate = this.sendingDate;
		tmp.deletionDate = this.deletionDate;
		tmp.content = this.content;
		return tmp;
	}

	@Override
	public String toString() {
		return "Message [messageId=" + messageId + ", senderService=" + senderService + ", msgInterface=" + msgInterface
				+ ", receiverService=" + receiverService + ", sendingDate=" + sendingDate + ", deletionDate="
				+ deletionDate + "]";
	}

}
