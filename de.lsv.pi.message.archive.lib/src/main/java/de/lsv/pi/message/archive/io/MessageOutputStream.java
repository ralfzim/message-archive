package de.lsv.pi.message.archive.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

public class MessageOutputStream extends OutputStream {

	private GZIPOutputStream stream;
	private ByteArrayOutputStream bos;

	public MessageOutputStream() throws IOException {
		bos = new ByteArrayOutputStream();
		stream = new GZIPOutputStream(new HEXOutputStream(bos));
	}
	
	@Override
	public void write(int b) throws IOException {
		this.stream.write(b);
	}

	public void close() throws IOException {
		stream.close();
	}

	public boolean equals(Object obj) {
		return stream.equals(obj);
	}

	public void finish() throws IOException {
		stream.finish();
	}

	public void flush() throws IOException {
		stream.flush();
	}

	public int hashCode() {
		return stream.hashCode();
	}

	public String toString() {
		return bos.toString();
	}

	public String toString(String coding) throws IOException {
		return bos.toString(coding);
	}

	public byte[] toByteArray() {
		return bos.toByteArray();
	}
	
	public void write(byte[] buf, int off, int len) throws IOException {
		stream.write(buf, off, len);
	}

	public void write(byte[] b) throws IOException {
		stream.write(b);
	}

}
