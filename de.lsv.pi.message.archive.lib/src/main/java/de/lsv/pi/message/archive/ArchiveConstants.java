package de.lsv.pi.message.archive;


public interface ArchiveConstants {
	String PU_NAME = "de.lsv.pi.message.archive.db.ejb";
	
	String DMT_senderServiceParty = "SSP";
	
	String DMT_receiverServiceParty = "RSP";

	
	String DMT_senderServiceName = "SSN";
	
	String DMT_receiverServiceName = "RSN";
	
	String DMT_senderInterfaceURI = "ITU";
	
	String DMT_receiverInterfaceURI = "";
	
	String DMT_senderInterfaceName = "ITN";
	
	String DMT_receiverInterfaceName = "";
	String NQ_DIM_FindByTypeAndValue = "Dimension.findByTypeAndValue";
}
