package de.lsv.pi.message.archive;

import java.io.Serializable;

public class Service  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String party;
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String name;
	@Override
	public String toString() {
		return "Service [party=" + party + ", name=" + name + "]";
	}
	
}
