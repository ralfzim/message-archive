package de.lsv.pi.message.archive;
import java.util.UUID;

import javax.ejb.Local;

@Local
public interface PersistMessageBeanLocal {
	void save(Message m);
	void reset(UUID m);
	void deleteOnFault(Message sapMain);
}
