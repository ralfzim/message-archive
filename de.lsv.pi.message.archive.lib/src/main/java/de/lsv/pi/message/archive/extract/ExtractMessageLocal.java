package de.lsv.pi.message.archive.extract;

import java.io.IOException;
import java.util.UUID;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface ExtractMessageLocal {
	
	byte[] getMessageContent(UUID uid) throws IOException;

	EntityManager getEntityManager();
}
