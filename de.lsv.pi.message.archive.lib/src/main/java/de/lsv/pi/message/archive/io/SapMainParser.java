package de.lsv.pi.message.archive.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.Interface;
import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.Service;


public class SapMainParser implements XMLStreamConstants {
	private static final String YYYY_MM_DD_T_HH_MM_SS_Z = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
	private final static Location trc = Location.getLocation(SapMainParser.class);
	private abstract static class SimpleParseXmlStructure implements
			ParseXmlStructure<SapMainParser> {

		public SimpleParseXmlStructure(String string) {
		}

		public void parse(XMLStreamReader r, SapMainParser obj)
				throws XMLStreamException {
			StringBuilder b = new StringBuilder();
			while (true) {
				switch (r.next()) {
				case CDATA:
				case CHARACTERS:
					b.append(r.getText());
					break;
				case END_ELEMENT:
					trc.debugT("set({0}) in {1}",new Object[]{b,obj});
					this.set(obj, b.toString());
					return;
				}
			}
		}

		protected abstract void set(SapMainParser obj, String value);

	}

	private abstract static class ServiceXmlStructure implements
			ParseXmlStructure<SapMainParser> {
		public void parse(XMLStreamReader r, SapMainParser obj)
				throws XMLStreamException {
			final Map<String, String> map = new HashMap<String, String>();
			PSX_DetailsParser.parse(r, map);
			String name = map.get("Service");
			String party = map.get("Party");
			Service service = new Service();
			service.setName(name);
			service.setParty(party);
			set(obj, service);
		}

		protected abstract void set(SapMainParser obj, Service value);
	}

	private interface ParseXmlStructure<P> {
		void parse(XMLStreamReader r, P obj) throws XMLStreamException;
	}

	private final static Map<String, ParseXmlStructure<SapMainParser>> psxDetails;
	private final static ParseXmlStructure<Map<String, String>> PSX_DetailsParser = new ParseXmlStructure<Map<String, String>>() {

		public void parse(XMLStreamReader r, Map<String, String> obj)
				throws XMLStreamException {
			StringBuilder b = new StringBuilder();
			String tmp = r.getLocalName();
			while (true) {
				switch (r.next()) {
				case START_ELEMENT:
					b = new StringBuilder();
					break;
				case CDATA:
				case CHARACTERS:
					b.append(r.getText());
					break;
				case END_ELEMENT:
					if (tmp.equals(r.getLocalName()))
						return;
					else {
						obj.put(r.getLocalName(), b.toString());
						b = new StringBuilder();
					}
				}
			}

		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_MessageClass = new SimpleParseXmlStructure(
			"MessageClass") {

		@Override
		public void set(SapMainParser obj, String value) {

		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_ProcessingMode = new SimpleParseXmlStructure(
			"ProcessingMode") {

		@Override
		public void set(SapMainParser obj, String value) {

		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_MessageId = new SimpleParseXmlStructure(
			"MessageId") {

		@Override
		public void set(SapMainParser obj, String value) {
			obj.messageId = UUID.fromString(value);
		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_TimeSent = new SimpleParseXmlStructure(
			"TimeSent") {

		@Override
		public void set(SapMainParser obj, String value) {
			try {
				SimpleDateFormat format = new SimpleDateFormat(
						YYYY_MM_DD_T_HH_MM_SS_Z);
				obj.sendingDate = format.parse(value);
			} catch (ParseException e) {
				throw new IllegalArgumentException(e);
			}

		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_Sender = new ServiceXmlStructure() {

		@Override
		public void set(SapMainParser obj, Service value) {
			obj.senderService = value;
		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_Receiver = new ServiceXmlStructure() {

		@Override
		public void set(SapMainParser obj, Service value) {
			obj.receiverService = value;

		}
	};
	private final static ParseXmlStructure<SapMainParser> PSX_Interface = new ParseXmlStructure<SapMainParser>() {

		public void parse(XMLStreamReader r, SapMainParser obj)
				throws XMLStreamException {
			final StringBuilder b = new StringBuilder();
			final String namespaceURI = r.getAttributeValue(null, "namespace");

			while (true) {
				switch (r.next()) {
				case CDATA:
				case CHARACTERS:
					b.append(r.getText());
					break;
				case END_ELEMENT:
					String name = b.toString();
					String uri = namespaceURI;
					Interface tmp = new Interface();
					tmp.setInterfaceName(name);
					tmp.setNamespaceURI(uri);
					obj.senderInterface = tmp ;
					return;
				}
			}

		}
	};
	static {
		Map<String, ParseXmlStructure<SapMainParser>> tmp = new HashMap<String, ParseXmlStructure<SapMainParser>>();
		tmp.put("MessageClass", PSX_MessageClass);
		tmp.put("ProcessingMode", PSX_ProcessingMode);
		tmp.put("MessageId", PSX_MessageId);
		tmp.put("TimeSent", PSX_TimeSent);
		tmp.put("Sender", PSX_Sender);
		tmp.put("Receiver", PSX_Receiver);
		tmp.put("Interface", PSX_Interface);
		psxDetails = Collections.unmodifiableMap(tmp);
	}
	private UUID messageId;
	private Interface receiverInterface;
	private Service receiverService;
	private Interface senderInterface;
	private Service senderService;
	private Date sendingDate;

	public static Message parseHeader(byte[] w)
			throws IOException {
		try {
			SapMainParser sapMain = new SapMainParser();
			XMLStreamReader reader = openReader(w);
			parse(reader, sapMain);

			Message tmp = new Message();
			tmp.setMsgInterface(sapMain.getInterface());
			tmp.setSenderService(sapMain.getSenderService());
			tmp.setReceiverService(sapMain.getReceiverService());
			tmp.setMessageId(sapMain.getMessageId());
			tmp.setSendingDate(sapMain.getSendingDate());
			tmp.setDeletionDate(sapMain.getDeletionDate());
			tmp.setContent(sapMain.getContent());
			return tmp;
		} catch (XMLStreamException e) {
			throw new IOException(e.getMessage(), e);
		}
	}
	private static void parse(XMLStreamReader reader, SapMainParser sapMain)
			throws XMLStreamException {
		while (true) {
			switch (reader.next()) {
			case START_ELEMENT:
				ParseXmlStructure<SapMainParser> tmp = psxDetails.get(reader
						.getLocalName());
				if (tmp != null) {
					tmp.parse(reader, sapMain);
				}
				break;
			case END_DOCUMENT:
				return;
			}
		}

	}

	private static XMLStreamReader openReader(byte[] w)
			throws XMLStreamException, FactoryConfigurationError, IOException {
		return XMLInputFactory//
				.newInstance()//
				.createXMLStreamReader(new ByteArrayInputStream(w));
	}

	public byte[] getContent() {
		return null;
	}

	public Date getDeletionDate() {
		return null;
	}

	public UUID getMessageId() {
		return this.messageId;
	}

	public Interface getReceiverInterface() {
		return this.receiverInterface;
	}

	public Service getReceiverService() {
		return this.receiverService;
	}

	public Interface getInterface() {
		return this.senderInterface;
	}

	public Service getSenderService() {
		return this.senderService;
	}

	public Date getSendingDate() {
		return this.sendingDate;
	}
}
