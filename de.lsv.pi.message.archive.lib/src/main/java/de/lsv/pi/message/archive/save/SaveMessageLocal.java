package de.lsv.pi.message.archive.save;

import java.io.IOException;

import javax.ejb.Local;

import de.lsv.pi.message.archive.query.SaveRequest;
import de.lsv.pi.message.archive.query.SaveResponse;

@Local
public interface SaveMessageLocal {
	public interface StatusLog {
		void init(int size);
		void saved(String id);
	}
	SaveResponse saveMessages(SaveRequest req,StatusLog log) throws IOException;
}
