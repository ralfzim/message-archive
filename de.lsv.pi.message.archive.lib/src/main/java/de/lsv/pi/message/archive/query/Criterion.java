package de.lsv.pi.message.archive.query;

import java.io.Serializable;

public class Criterion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String low,high,operator,field,type;
	public final static String
		TYPE_INCLUDE = "I", //
		TYPE_EXCLUDE = "E", //
		OP_EQ = "EQ", //
		OP_GT = "GT", //
		OP_LT = "LT", //
		OP_GE = "GE", //
		OP_LE = "LE", //
		OP_BW = "BW"; //
	
	public String getLow() {
		return low;
	}

	public String getHigh() {
		return high;
	}

	public String getOperator() {
		return operator;
	}

	public String getField() {
		return field;
	}

	public String getType() {
		return type;
	}

	public Criterion setLow(String low) {
		this.low = low;
		return this;
	}

	public Criterion setHigh(String high) {
		this.high = high;
		return this;
	}

	public Criterion setOperator(String operator) {
		this.operator = operator;
		return this;
	}

	public Criterion setField(String field) {
		this.field = field;
		return this;
	}

	public Criterion setType(String type) {
		this.type = type;
		return this;
	}

	public Criterion negotiate(String op) {
		Criterion tmp = new Criterion();
		tmp.setField(field);
		tmp.setHigh(high);
		tmp.setLow(low);
		tmp.setType(type);
		tmp.setOperator(op);
		return tmp ;
	}

	@Override
	public String toString() {
		return "Criterion [field=" + field + ", operator=" + operator + ", type=" + type + ", low=" + low + ", high="
				+ high + "]";
	}
	
}
