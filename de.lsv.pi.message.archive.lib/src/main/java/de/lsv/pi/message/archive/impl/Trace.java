package de.lsv.pi.message.archive.impl;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class Trace {

	public static Object[] toObject(Object... o) {
		List<Object> tmp = new ArrayList<Object>(o.length);
		for (Object i : o) {
			if (i instanceof Throwable) {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				PrintWriter w = new PrintWriter(bos);
				((Throwable) i).printStackTrace(w);
				w.close();
				tmp.add(bos.toString());
			} else
				tmp.add(i);
		}
		return tmp.toArray(new Object[tmp.size()]);
	}

}
