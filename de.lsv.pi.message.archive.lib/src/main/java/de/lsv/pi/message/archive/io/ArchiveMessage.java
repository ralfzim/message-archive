package de.lsv.pi.message.archive.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

import de.lsv.pi.message.archive.Message;

public class ArchiveMessage {
	private static final QName QN_SAP_MAIN = new QName("http://sap.com/xi/XI/Message/30", "Main");
	private static String CONTENT_TYPE_TEMPLATE = "multipart/related; boundary=\"%s\"";
	private AttachmentPart payload;
	private List<AttachmentPart> attachments = new LinkedList<AttachmentPart>();
	private SOAPMessage msg;
	private Message sapMain;
	private ArchiveMessage(SOAPMessage msg) throws SOAPException {
		this.msg = msg;
		sortAttachments(msg);
	}

	private void sortAttachments(SOAPMessage msg) {
		for (@SuppressWarnings("unchecked")
		Iterator<AttachmentPart> atts = msg.getAttachments();atts.hasNext();) {
			AttachmentPart next = atts.next();
			if ("MainDocument".equals(next.getContentId()))
				this.payload = next;
			else
				attachments.add(next);
		}
	}

	public static ArchiveMessage read(byte[] content) throws IOException {
		MimeHeaders headers = new MimeHeaders();
		addBoundary(headers,content);
		InputStream in = new ByteArrayInputStream(content);
		SOAPMessage msg;
		try {
			msg = MessageFactory.newInstance().createMessage(headers, in);
		}  catch (SOAPException e) {
			throw new IOException(e);
		}
		try {
			return new ArchiveMessage(msg);
		} catch (SOAPException e) {
			throw new IOException("",e);
		}
	}

	private static void addBoundary(MimeHeaders headers, byte[] content) {
		int i = 0;
		for (byte b =content[i];b != '\n' && b != '\r';b = content[++i]);
		String boundary = new String(content,2,i-2);
		headers.addHeader("Content-Type", String.format(CONTENT_TYPE_TEMPLATE,boundary));
	}
	
	public byte[] getMainDocument() throws IOException {
		try {
			return payload.getRawContentBytes();
		} catch (SOAPException e) {
			throw new IOException(e);
		}
	}
	
	public ListIterator<AttachmentPart> getAttachments() {
		return (ListIterator<AttachmentPart>) this.attachments.iterator();
	}
	
	public Message getSapMain() throws IOException {
		if (this.sapMain == null) {
			try {
				parseSapMain();
			} catch (Exception e) {
				throw new IOException("",e);
			}

		}
		return this.sapMain ;
	}

	private void parseSapMain() throws SOAPException, TransformerException, TransformerFactoryConfigurationError, IOException {
		@SuppressWarnings("unchecked")
		Iterator<Element> main = msg.getSOAPHeader().getChildElements(QN_SAP_MAIN);
		if (main.hasNext()) {
			Element m = main.next();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(new DOMSource(m), new StreamResult(bos));
			this.sapMain = SapMainParser.parseHeader(bos.toByteArray());
		} else {
			throw new SOAPException();
		}
	}
}
