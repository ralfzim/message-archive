package de.lsv.pi.message.archive.query;

public abstract class AbstractRequest {
	static final Criterion[] EMPTY_CRITERIONS = new Criterion[0];

	private Criterion[] criterions;

	public Criterion[] getCriterions() {
		if (criterions == null)
			criterions = EMPTY_CRITERIONS;
		return criterions;
	}

	public void setCriterions(Criterion[] criterions) {
		this.criterions = criterions;
	}

}
