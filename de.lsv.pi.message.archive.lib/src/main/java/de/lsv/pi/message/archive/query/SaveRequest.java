package de.lsv.pi.message.archive.query;

import java.io.Serializable;
import java.util.Arrays;

public class SaveRequest extends AbstractRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4091559041439497164L;

	private String pathToSave;
	private int length;
	private int offset;

	public void setPathToSave(String pathToSave) {
		this.pathToSave = pathToSave;
	}

	public String getPathToSave() {
		return pathToSave;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "SaveRequest [pathToSave=" + pathToSave + ", length=" + length + ", offset=" + offset
				+ ", getCriterions()=" + Arrays.toString(getCriterions()) + "]";
	}

	
	
}
