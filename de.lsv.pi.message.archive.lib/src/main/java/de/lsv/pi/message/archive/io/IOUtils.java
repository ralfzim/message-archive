package de.lsv.pi.message.archive.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {

	public static void copyWithClose(InputStream is, OutputStream os)
			throws IOException {
		byte[] b = new byte[4096 * 4];
		int len, off = 0;
		while ((len = is.read(b)) > 0)
			os.write(b, off, len);
		try {
			is.close();
		} catch (IOException e) {
		}
		try {
			os.close();
		} catch (IOException e) {
		}
	}
	
	public static String toString(InputStream is) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		copyWithClose(is, bos);
		return bos.toString();
	}

	public static byte[] toByteArray(InputStream is) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		copyWithClose(is, bos);
		return bos.toByteArray();
	}
}
