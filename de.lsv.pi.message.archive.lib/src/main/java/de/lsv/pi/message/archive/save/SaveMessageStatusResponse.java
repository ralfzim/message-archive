package de.lsv.pi.message.archive.save;

public class SaveMessageStatusResponse extends SaveMessageStatusRequest{
	public enum EvaluationState {
		NoFile, WrongFileContent, IoException, OK, NumberFormat;
	}
	private int actualAmount, maxAmount;
	private EvaluationState evaluationState;

	public int getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(int actualAmount) {
		this.actualAmount = actualAmount;
	}

	public int getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(int maxAmount) {
		this.maxAmount = maxAmount;
	}

	@Override
	public String toString() {
		return "SaveMessageStatusResponse [actualAmount=" + actualAmount + ", maxAmount=" + maxAmount
				+ ", getActualAmount()=" + getActualAmount() + ", getMaxAmount()=" + getMaxAmount() + "]";
	}

	public EvaluationState getEvaluationState() {
		return evaluationState;
	}

	public void setEvaluationState(EvaluationState evaluationState) {
		this.evaluationState = evaluationState;
	}
	
}
