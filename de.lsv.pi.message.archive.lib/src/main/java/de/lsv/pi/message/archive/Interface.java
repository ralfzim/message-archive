package de.lsv.pi.message.archive;

import java.io.Serializable;

public class Interface  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String namespaceURI;
	private String interfaceName;
	public String getNamespaceURI() {
		return namespaceURI;
	}
	public void setNamespaceURI(String namespaceURI) {
		this.namespaceURI = namespaceURI;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	@Override
	public String toString() {
		return "Interface [namespaceURI=" + namespaceURI + ", interfaceName=" + interfaceName + "]";
	}
	
}
