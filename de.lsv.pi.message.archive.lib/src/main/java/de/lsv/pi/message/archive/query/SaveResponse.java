package de.lsv.pi.message.archive.query;

import java.io.Serializable;

public class SaveResponse extends AbstractResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6861786221267462393L;
	private String processUUID;
	private String path;
	public String getProcessUUID() {
		return processUUID;
	}
	public void setProcessUUID(String processUUID) {
		this.processUUID = processUUID;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

}
