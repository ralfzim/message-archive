package de.lsv.pi.message.archive.save;

import javax.ejb.Local;

@Local
public interface SaveMessageStatus {
	SaveMessageStatusResponse updateStatus(SaveMessageStatusRequest req);
}
