package de.lsv.pi.message.archive.io;

import java.io.IOException;
import java.io.OutputStream;

public class HEXOutputStream extends OutputStream {

	private OutputStream os;

	public HEXOutputStream(OutputStream os) {
		this.os = os;
	}

	final static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
			'z' };
	private final int shift = 4;
	private final int radix = 1 << shift;
	private final int mask = radix - 1;

	@Override
	public void write(int b) throws IOException {
		byte[] buf = new byte[2];
		int c = (b & 0xff);
		int pos = 2;
		do {
			buf[--pos] = (byte) digits[c & mask];
		    c >>>= shift;
		} while (pos > 0);
		c >>>= shift;
		this.os.write((byte[]) buf);
	}

}
