package de.lsv.pi.message.archive.job;

import javax.ejb.Stateless;

@Stateless
public interface UpdateMessageDateJob {
	
	JobResponse updateMessages(JobRequest req);
	JobResponse statusUpdateMessage(JobStatusRequest req);

}
