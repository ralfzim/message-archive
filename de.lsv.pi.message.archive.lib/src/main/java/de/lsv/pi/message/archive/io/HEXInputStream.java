package de.lsv.pi.message.archive.io;

import java.io.IOException;
import java.io.InputStream;

public class HEXInputStream extends InputStream {
	boolean open = true;
	private InputStream source;
	public HEXInputStream(InputStream source) {
		this.source = source;
	}
	@Override
	public int read() throws IOException {
		if (!open)
			return -1;
		int result = 0;
		int i = this.source.read();
		if (i < 0) {
			open = false;
			return -1;
		}
		result = Character.digit((char)i, 16);
		i = this.source.read();
		if (i <= 0)
			open = false;
		else {
			int tmp =  Character.digit((char)i, 16);
			result = (result << 4)| tmp;
		}
		return result;
	}

}
