package de.lsv.pi.message.archive.query;

public class SearchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SearchException() {
		// TODO Auto-generated constructor stub
	}

	public SearchException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SearchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SearchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
