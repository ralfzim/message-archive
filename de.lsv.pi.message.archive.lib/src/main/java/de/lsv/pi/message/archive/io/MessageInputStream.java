package de.lsv.pi.message.archive.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class MessageInputStream extends InputStream {

	private GZIPInputStream stream;
	public MessageInputStream(byte[] bytes) throws IOException {
		this.stream = new GZIPInputStream(new HEXInputStream(new ByteArrayInputStream(bytes)));
	}
	@Override
	public int read() throws IOException {
		return this.stream.read();
	}
	public int available() throws IOException {
		return stream.available();
	}
	public void close() throws IOException {
		stream.close();
	}
	public boolean equals(Object obj) {
		return stream.equals(obj);
	}
	public int hashCode() {
		return stream.hashCode();
	}
	public void mark(int readlimit) {
		stream.mark(readlimit);
	}
	public boolean markSupported() {
		return stream.markSupported();
	}
	public int read(byte[] buf, int off, int len) throws IOException {
		return stream.read(buf, off, len);
	}
	public int read(byte[] b) throws IOException {
		return stream.read(b);
	}
	public void reset() throws IOException {
		stream.reset();
	}
	public long skip(long n) throws IOException {
		return stream.skip(n);
	}

}
