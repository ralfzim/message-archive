package de.lsv.pi.message.archive.query;

import java.util.Collection;
import javax.ejb.Local;
@Local
public interface QueryMessageLocal {
	public class SearchEntity {
		private String name;
		private String field;

		public SearchEntity(String name,String field) {
			this.name = name;
			this.field = field;
		}

		public String getName() {
			return name;
		}

		public String getField() {
			return field;
		}
		
	}
	SearchResponse search(SearchRequest request) throws SearchException;
	

	void delete(AbstractRequest searchReq) throws SearchException;
	
	Collection<SearchEntity> listCategories() throws SearchException;

	Collection<SearchEntity> listOperators() throws SearchException;
}
