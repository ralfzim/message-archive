package de.lsv.pi.message.archive.query;

import java.io.Serializable;
import java.util.Arrays;

public class SearchRequest extends AbstractRequest  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -298105067008917788L;
	private int start,length;
	public void setStart(int start) {
		this.start = start;
	}

	public int getStart() {
		return start;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	@Override
	public String toString() {
		return "SearchRequest [start=" + start + ", length=" + length + ", getCriterions()="
				+ Arrays.toString(getCriterions()) + "]";
	}
	
}
