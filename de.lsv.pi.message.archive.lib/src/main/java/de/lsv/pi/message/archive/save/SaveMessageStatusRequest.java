package de.lsv.pi.message.archive.save;

public class SaveMessageStatusRequest {
	private String processID;
	private String path;
	public String getProcessID() {
		return processID;
	}
	public void setProcessID(String processID) {
		this.processID = processID;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@Override
	public String toString() {
		return "SaveMessageStatusRequest [processID=" + processID + ", path=" + path + "]";
	}
	
}
