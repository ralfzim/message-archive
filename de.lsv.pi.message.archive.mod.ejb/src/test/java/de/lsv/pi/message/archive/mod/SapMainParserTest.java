package de.lsv.pi.message.archive.mod;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import de.lsv.pi.edi.adapters.sdk.message.MessageHeaderWrapper;
import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.io.SapMainParser;

public class SapMainParserTest {

	@Test
	public void testParseHeader() throws IOException {
		MessageHeaderWrapper w = load("sap_main.xml");
		Message main = SapMainParser.parseHeader(w.getContent() );
		assertNotNull(main);
		assertNotNull("MessageId is Null",main.getMessageId());
		assertNotNull("SenderService is Null",main.getSenderService());
		assertNotNull(main.getSenderService().getParty());
		assertNotNull(main.getSenderService().getName());
		assertNotNull("ReceiverService is Null",main.getReceiverService());
		assertNotNull("Interface is Null",main.getMsgInterface());
		assertNotNull("SendingDate is Null",main.getSendingDate());
		assertNull(main.getContent());
	}
	
	@Test
	public void testObjectStream() throws IOException, ClassNotFoundException {
		MessageHeaderWrapper w = load("sap_main.xml");
		Message main = SapMainParser.parseHeader(w.getContent() );
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream objOut = new ObjectOutputStream(out);
		objOut.writeObject(main);
		objOut.close();
		InputStream in = new ByteArrayInputStream(out.toByteArray());
		ObjectInputStream objIn = new ObjectInputStream(in);
		Message readed = (Message) objIn.readObject();
		assertEquals(main.getMessageId(), readed.getMessageId());
	}	
	
//	@Test
//	public void testPersistMessageFactory() throws IOException {
//		MessageHeaderWrapper w = load("sap_main.xml");
//		Message main = SapMainParser.parseHeader(w );
//		main = PersistMessageFactory.newInstance().create(main, "mainDocumentContent".getBytes());
//		assertNotNull(main);
//		assertNotNull("MessageId is Null",main.getMessageId());
//		assertNotNull("SenderService is Null",main.getSenderService());
//		assertNotNull(main.getSenderService().getParty());
//		assertNotNull(main.getSenderService().getName());
//		assertNotNull("ReceiverService is Null",main.getReceiverService());
//		assertNotNull("Interface is Null",main.getInterface());
//		assertNotNull("SendingDate is Null",main.getSendingDate());
//		assertNotNull(main.getContent());
//	}
	

	private MessageHeaderWrapper load(String string) throws IOException {
		InputStream is = getClass().getResourceAsStream(string);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buf = new byte[4096];
		int len, off = 0;
		while((len = is.read(buf))> 0) {
			os.write(buf,off,len);
		}
		is.close();
		os.close();
		final byte[] content = os.toByteArray();
		return new MessageHeaderWrapper() {
			
			public void setContent(byte[] bytes) throws IOException {
				// TODO Auto-generated method stub
				
			}
			
			public String getNamespaceURI() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public String getName() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
			
			public byte[] getContent() throws IOException {
				return content;
			}
		};
	}

}
