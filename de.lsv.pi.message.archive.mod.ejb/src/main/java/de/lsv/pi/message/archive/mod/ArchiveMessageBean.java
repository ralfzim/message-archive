package de.lsv.pi.message.archive.mod;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sap.aii.af.lib.mp.module.Module;
import com.sap.aii.af.lib.mp.module.ModuleContext;
import com.sap.aii.af.lib.mp.module.ModuleData;
import com.sap.aii.af.lib.mp.module.ModuleException;
import com.sap.aii.af.lib.mp.module.SModule;

import de.lsv.pi.edi.adapters.sdk.common.Iterators;
import de.lsv.pi.edi.adapters.sdk.ctx.ModuleContextUtil;
import de.lsv.pi.edi.adapters.sdk.message.MessageHeaderWrapper;
import de.lsv.pi.edi.adapters.sdk.message.MessageWrapper;
import de.lsv.pi.edi.adapters.sdk.message.MessageWrapperFactory;
import de.lsv.pi.edi.adapters.sdk.trace.ModuleTrace;
import de.lsv.pi.edi.adapters.sdk.trace.ModuleTraceFactory;
import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.PersistMessageBeanLocal;
import de.lsv.pi.message.archive.io.SapMainParser;




/**
 * The PayloadAsAttachmentBean takes the Payload as Attachment and send an empty
 * xml file as payload</b>.
 * 
 */
public class ArchiveMessageBean implements SessionBean, Module, SModule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 20121204009L;
	private final static String JNDI_PMBL = "lsv.de/LSV_MESSAGE_ARCHIVE/LOCAL/PersistMessageBean" +
			"/de.lsv.pi.message.archive.PersistMessageBeanLocal";
	public void ejbActivate() throws EJBException, RemoteException {
	}

	public void ejbPassivate() throws EJBException, RemoteException {
	}

	public void ejbRemove() throws EJBException, RemoteException {
	}

	public void setSessionContext(SessionContext arg0) throws EJBException,
			RemoteException {
	}

	public ModuleData process(ModuleContext moduleContext,
			ModuleData inputModuleData) throws ModuleException {
		ModuleTrace trace = ModuleTraceFactory.newTrace(getClass(),
				inputModuleData);
		trace.entering();
		Map<String, String> config = ModuleContextUtil.buildMap(moduleContext);
		final MessageWrapper message = MessageWrapperFactory
				.newMessageWrapper(inputModuleData);
		try {
			MessageHeaderWrapper mainHeader = Iterators.filter(ModuleDefinitions.findSapMain, message.getHeaders()).next();
			trace.debug("SapMain as header",mainHeader);
			Message sapMain = SapMainParser.parseHeader(mainHeader.getContent());
			sapMain = PersistMessageFactory.newInstance().create(sapMain,message);
			sapMain = PersistMessageFactory.newInstance().calculateDeletionDate(sapMain,config);
			trace.debug("SapMain as message",sapMain);
			PersistMessageBeanLocal local = getPersistMessageBean();
			local.save(sapMain);
		} catch (IOException e) {
			trace.catching("ArchiveMessageBean error during message split!", e);
			throw new ModuleException(e);
		} catch (NamingException e) {
			trace.catching("ArchiveMessageBean error during message split!", e);
			throw new ModuleException(e);
		} 
		trace.leaving();
		return inputModuleData;
	}

	private PersistMessageBeanLocal getPersistMessageBean() throws NamingException {
		InitialContext ctx = new InitialContext();
		PersistMessageBeanLocal local = (PersistMessageBeanLocal) ctx.lookup(JNDI_PMBL);
		return local;
	}

	public void ejbCreate() throws CreateException {

	}

	@Override
	public ModuleData onFault(ModuleContext moduleContext, ModuleData inputModuleData,
			Exception arg2) {
		ModuleTrace trace = ModuleTraceFactory.newTrace(getClass(),
				inputModuleData);
		trace.entering();
		final MessageWrapper message = MessageWrapperFactory
				.newMessageWrapper(inputModuleData);
		try {
			MessageHeaderWrapper mainHeader = Iterators.filter(ModuleDefinitions.findSapMain, message.getHeaders()).next();
			trace.debug("SapMain as header",mainHeader);
			Message sapMain = SapMainParser.parseHeader(mainHeader.getContent());
			PersistMessageBeanLocal local = getPersistMessageBean();
			local.deleteOnFault(sapMain);
		} catch (Exception e) {
			
		}
		
		return inputModuleData;
	}
}
