package de.lsv.pi.message.archive.mod;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import system.sbl.monad.Either;
import system.sbl.monad.MonadFunctions;
import system.sbl.monad.Parameter;
import system.sbl.monad.ParameterList;

import com.sap.tc.logging.Location;

import de.lsv.pi.edi.adapters.sdk.message.MessageAttachmentWrapper;
import de.lsv.pi.edi.adapters.sdk.message.MessageHeaderWrapper;
import de.lsv.pi.edi.adapters.sdk.message.MessageWrapper;
import de.lsv.pi.message.archive.Message;
import de.lsv.pi.message.archive.impl.Trace;
import de.lsv.pi.message.archive.io.MessageOutputStream;

public abstract class PersistMessageFactory {

	class BinaryDataSource implements DataSource {
		InputStream _is;

		public BinaryDataSource(byte[] content) {
			this(new ByteArrayInputStream(content));
		}

		public BinaryDataSource(InputStream is) {
			_is = is;
		}

		public String getContentType() {
			return APPLICATION_BINARY;
		}

		public InputStream getInputStream() throws IOException {
			return _is;
		}

		public String getName() {
			return "some file";
		}

		public OutputStream getOutputStream() throws IOException {
			throw new IOException("Cannot write to this file");
		}
	}

	private static class RetentionPeriodDefinition {
		private int calendarField;
		private int summand;

		public RetentionPeriodDefinition(int calendarField, int summand) {
			super();
			this.calendarField = calendarField;
			this.summand = summand;
		}

		public int getCalendarField() {
			return calendarField;
		}

		public int getSummand() {
			return summand;
		}

	}

	private static class RetentionPeriodEvaluation implements
			Either.Function<RetentionPeriodDefinition, Map<String, String>> {

		private static final String INVALID_VALUE_S_FOR_PARAMETER_S = "Cann't calculate retention period!\n Invalid value %s for parameter %s";
		private int calendarField;
		private String parameterName;

		public RetentionPeriodEvaluation(String parameterName, int i) {
			this.parameterName = parameterName;
			this.calendarField = i;
		}

		@SuppressWarnings("unchecked")
		public Either<RetentionPeriodDefinition, Map<String, String>> apply(
				ParameterList p) {
			Map<String, String> prop = (Map<String, String>) p.next(Map.class);
			if (prop.containsKey(parameterName)) {
				String paramValue = prop.get(parameterName);
				try {
					int period = Integer.parseInt(paramValue);
					return Either.FACTORY.left(new RetentionPeriodDefinition(
							calendarField, period));
				} catch (NumberFormatException e) {
					String message = String.format(
							INVALID_VALUE_S_FOR_PARAMETER_S, paramValue,
							parameterName);
					throw new RuntimeException(message, e);
				}
			} else
				return Either.FACTORY.right(prop);
		}

	}

	private static final String APPLICATION_BINARY = "application/octet-stream";
	private static final String CANN_T_CALCULATE_RETENTION = "Cann't calculate retention period!\nMissing parameter %s or %s in module configuration.";
	private static final String PARAMETER_ARCHIVE_ANNO = "retention.period.anno";
	private static final String PARAMETER_ARCHIVE_DAYS = "retention.period.days";
	private final static RetentionPeriodEvaluation //
			EVAL_ANNO_PARAMETER = new RetentionPeriodEvaluation(
					PARAMETER_ARCHIVE_ANNO, Calendar.YEAR),
			EVAL_DAYS_PARAMETER = new RetentionPeriodEvaluation(
					PARAMETER_ARCHIVE_DAYS, Calendar.DAY_OF_YEAR);

	private static final PersistMessageFactory INSTANCE = new PersistMessageFactory() {

		private void addHeader(SOAPMessage msg, MessageHeaderWrapper next)
				throws DOMException, SOAPException,
				ParserConfigurationException, SAXException, IOException {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new ByteArrayInputStream(next
					.getContent()));
			Node importedNode = doc.getFirstChild();
			Node newNode = msg.getSOAPHeader().getOwnerDocument().importNode(
					importedNode, true);
			msg.getSOAPHeader().appendChild(newNode);
		}

		private void appendAttachment(SOAPMessage msg,
				MessageAttachmentWrapper next) throws IOException {
			DataHandler dh = new DataHandler(new BinaryDataSource(next
					.getContent()));
			AttachmentPart part = msg.createAttachmentPart(dh);
			part.setContentId(next.getName());
			msg.addAttachmentPart(part);
		}

		private void appendAttachment(SOAPMessage msg, String string,
				byte[] mainDocumentContent) {
			DataHandler dh = new DataHandler(new BinaryDataSource(
					mainDocumentContent));
			AttachmentPart part = msg.createAttachmentPart(dh);
			part.setContentId("MainDocument");
			msg.addAttachmentPart(part);
		}

		@Override
		public Message calculateDeletionDate(Message sapMain,
				Map<String, String> config) {
			RetentionPeriodDefinition days = calculatePeriod(config);
			final Date deletionDate = sapMain.getSendingDate();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(deletionDate);
			calendar.add(days.getCalendarField(), days.getSummand());
			Message tmp = sapMain.cloneMsg();
			tmp.setDeletionDate(calendar.getTime());
			return tmp;
		}

		private RetentionPeriodDefinition calculatePeriod(
				Map<String, String> config) {
			Parameter predecessor = Parameter.FACTORY.predecessor(0);
			return Either.FACTORY
					.<RetentionPeriodDefinition, Map<String, String>> //
					right(config)//
					.bind(EVAL_ANNO_PARAMETER, predecessor)//
					.bind(EVAL_DAYS_PARAMETER, predecessor) //
					.onRight(THROW_MISSING_PARAMETER_EXCEPTION) //
					.message();
		}

		@Override
		public Message create(final Message sapMain,
				final MessageWrapper message) throws IOException {
			Message tmp = sapMain.cloneMsg();
			try {
				SOAPMessage msg = MessageFactory.newInstance().createMessage();
				for (Iterator<MessageHeaderWrapper> h = message.getHeaders(); //
				h.hasNext(); //
				addHeader(msg, h.next()))
					;
				appendAttachment(msg, "MainDocument", message
						.getMainDocumentContent());
				for (Iterator<MessageAttachmentWrapper> i = message
						.getAttachments(); i.hasNext(); //
				appendAttachment(msg, i.next()))
					;
				MessageOutputStream bos = new MessageOutputStream();
				msg.writeTo(bos);
				bos.close();
				tmp.setContent(bos.toByteArray());
			} catch (SOAPException e) {
				loc.errorT("create", "SOAPException {0}", Trace.toObject(e));
				throw new IOException(e.getLocalizedMessage());
			} catch (DOMException e) {
				loc.errorT("create", "DOMException {0}", Trace.toObject(e));
				throw new IOException(e.getLocalizedMessage());
			} catch (ParserConfigurationException e) {
				loc.errorT("create", "ParserConfigurationException {0}", Trace
						.toObject(e));
				throw new IOException(e.getLocalizedMessage());
			} catch (SAXException e) {
				loc.errorT("create", "SAXException {0}", Trace.toObject(e));
				throw new IOException(e.getLocalizedMessage());
			}

			return tmp;
		}
	};

	private final static Location loc = Location
			.getLocation(PersistMessageFactory.class);

	private static final MonadFunctions.Function<Object> THROW_MISSING_PARAMETER_EXCEPTION = new MonadFunctions.Function<Object>() {

		private final String message = String.format(
				CANN_T_CALCULATE_RETENTION, PARAMETER_ARCHIVE_ANNO,
				PARAMETER_ARCHIVE_DAYS);

		public Object apply(ParameterList parameters) {
			throw new RuntimeException(message);
		}
	};

	public static PersistMessageFactory newInstance() {
		return INSTANCE;
	}

	public abstract Message calculateDeletionDate(Message sapMain,
			Map<String, String> config);

	public abstract Message create(Message sapMain, MessageWrapper message)
			throws IOException;

}
