package de.lsv.pi.message.archive.mod;

import java.io.IOException;

import de.lsv.pi.edi.adapters.sdk.common.Iterators;
import de.lsv.pi.edi.adapters.sdk.message.MessageHeaderWrapper;

interface ModuleDefinitions {

	String NSURI_SAP = "http://sap.com/xi/XI/Message/30";
	String HE_MAIN = "Main";
	Iterators.Function<MessageHeaderWrapper, Boolean> findSapMain = new Iterators.Function<MessageHeaderWrapper, Boolean>() {
		
		public Boolean map(MessageHeaderWrapper a) {
			try {
				return NSURI_SAP.equals(a.getNamespaceURI()) && HE_MAIN.equals(a.getName());
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}
	};
}
