package de.lsv.pi.message.archive.db.web;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.query.AbstractRequest;
import de.lsv.pi.message.archive.query.QueryMessageLocal;
import de.lsv.pi.message.archive.query.SearchException;
import de.lsv.pi.message.archive.query.SearchRequest;
import de.lsv.pi.message.archive.query.SearchResponse;

/**
 * Servlet implementation class ArchiveMessages
 */
public class ArchiveMessages extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final static Location loc = Location.getLocation(ArchiveMessages.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArchiveMessages() {
        super();
    }

    
    @EJB
    private QueryMessageLocal queryBean;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loc.entering();
		SearchResponse tmp;
		try {
			SearchRequest searchReq = readGetRequest(request);
			tmp = queryBean.search(searchReq);
			Gson gson = new Gson();
			String jsonString = gson.toJson(tmp);
			response.setStatus(200);
			response.setContentType("application/json");
			response.getWriter().append(jsonString);
		} catch (SearchException e) {
			throw new ServletException(e);
		} finally {
			loc.exiting();
		}
	}

	private SearchRequest readGetRequest(HttpServletRequest request) {
		SearchRequest tmp = new SearchRequest();
		return tmp ;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loc.entering();
		Gson gson = new Gson();
		SearchRequest searchReq = gson.fromJson(new InputStreamReader(request.getInputStream()), SearchRequest.class);
		SearchResponse tmp;
		try {
			tmp = queryBean.search(searchReq);
			response.setStatus(200);
			response.setContentType("application/json");
			gson = new GsonBuilder().setDateFormat("yyyyMMdd'T'HHmmss").create();
			String jsonString = gson.toJson(tmp);
			response.getWriter().append(jsonString);
		} catch (SearchException e) {
			throw new ServletException(e);
		} finally {
			loc.exiting();
		}
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loc.entering();
		Gson gson = new Gson();
		AbstractRequest searchReq = gson.fromJson(new InputStreamReader(request.getInputStream()), SearchRequest.class);
		try {
			queryBean.delete(searchReq);
			response.setStatus(200);
			response.setContentType("application/json");
			String jsonString = gson.toJson("SUCCESS");
			response.getWriter().append(jsonString);
		} catch (SearchException e) {
			throw new ServletException(e);
		} finally {
			loc.exiting();
		}
	}

}
