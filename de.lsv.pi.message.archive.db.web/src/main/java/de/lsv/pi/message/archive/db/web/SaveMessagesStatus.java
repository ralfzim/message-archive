package de.lsv.pi.message.archive.db.web;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.save.SaveMessageStatus;
import de.lsv.pi.message.archive.save.SaveMessageStatusRequest;
import de.lsv.pi.message.archive.save.SaveMessageStatusResponse;

/**
 * Servlet implementation class SaveMessage
 */
public class SaveMessagesStatus extends HttpServlet {



	private static final long serialVersionUID = 1L;
	private final static Location loc = Location.getLocation(SaveMessagesStatus.class);
      
	@EJB
	private SaveMessageStatus saveBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveMessagesStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loc.entering();
		Gson gson = new Gson();
		SaveMessageStatusRequest saveReq = gson.fromJson(new InputStreamReader(request.getInputStream()), SaveMessageStatusRequest.class);
		SaveMessageStatusResponse tmp;
		try {
			tmp = saveBean.updateStatus(saveReq);
			response.setStatus(200);
			response.setContentType("application/json");
			gson = new GsonBuilder().setDateFormat("yyyyMMdd'T'HHmmss").create();
			String jsonString = gson.toJson(tmp);
			response.getWriter().append(jsonString);
		} catch (IOException e) {
			loc.errorT("Error during processing: {0}",new Object[]{e});
			throw new ServletException(e);
		} finally {
			loc.exiting();
		}
	}

}
