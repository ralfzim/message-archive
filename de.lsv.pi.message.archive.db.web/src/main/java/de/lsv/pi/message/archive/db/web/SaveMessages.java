package de.lsv.pi.message.archive.db.web;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sap.tc.logging.Location;

import de.lsv.pi.message.archive.query.SaveRequest;
import de.lsv.pi.message.archive.query.SaveResponse;
import de.lsv.pi.message.archive.save.SaveMessageLocal;
import de.lsv.pi.message.archive.save.SaveMessageLocal.StatusLog;

/**
 * Servlet implementation class SaveMessage
 */
public class SaveMessages extends HttpServlet {
	private final class DoNothingStatusLog implements StatusLog {

		@Override
		public void init(int size) {
		}

		@Override
		public void saved(String id) {
		}
	}



	private static final long serialVersionUID = 1L;
	private final static Location loc = Location.getLocation(SaveMessages.class);
      
	@EJB
	private SaveMessageLocal saveBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveMessages() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loc.entering();
		Gson gson = new Gson();
		SaveRequest saveReq = gson.fromJson(new InputStreamReader(request.getInputStream()), SaveRequest.class);
		SaveResponse tmp;
		try {
			tmp = saveBean.saveMessages(saveReq,new DoNothingStatusLog());
			response.setStatus(200);
			response.setContentType("application/json");
			gson = new GsonBuilder().setDateFormat("yyyyMMdd'T'HHmmss").create();
			String jsonString = gson.toJson(tmp);
			response.getWriter().append(jsonString);
		} catch (IOException e) {
			loc.errorT("Error during processing: {0}",new Object[]{e});
			throw new ServletException(e);
		} finally {
			loc.exiting();
		}
	}

}
