package de.lsv.pi.message.archive.db.web;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.tc.logging.Location;

public class DeletePathServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Location loc = Location.getLocation(DeletePathServlet.class);
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathName = req.getPathInfo().split("\\?")[0];
		loc.errorT("Try to delete {0}",new Object[]{pathName});
		Path path = Paths.get("/transfer",pathName);
		if (!pathName.contains("..") && path.getNameCount() > 4 && req.getParameter("password").equals("Wichtig")) {
			loc.errorT("Try to delete {0}",new Object[]{path});
			deletePath(path);
		}
		
		resp.getWriter().append("OK");
	}
	private void deletePath(Path path) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(()->{deleteRecursive(path);});
		executor.shutdown();
	}
	private void deleteRecursive(Path path) {
		try {
			Files.walkFileTree(path, new FileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
