package de.lsv.pi.message.archive.db.web;

import java.io.IOException;
import java.util.Collection;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import de.lsv.pi.message.archive.query.QueryMessageLocal;
import de.lsv.pi.message.archive.query.SearchException;
import de.lsv.pi.message.archive.query.QueryMessageLocal.SearchEntity;

/**
 * Servlet implementation class MetadataServlet
 */
public class OperatorMetadataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OperatorMetadataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    @EJB
    private QueryMessageLocal queryBean;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Collection<SearchEntity> categories = queryBean.listOperators();
			Gson gson = new Gson();
			String jsonString = gson.toJson(categories);
			response.setStatus(200);
			response.setContentType("application/json");
			response.getWriter().append(jsonString);
		} catch (SearchException e) {
			throw new ServletException("Error during Metadata request", e);
		}
	}

}
