package de.lsv.pi.message.archive.db.web;

import java.io.IOException;
import java.util.UUID;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.lsv.pi.message.archive.extract.ExtractMessageLocal;

/**
 * Servlet implementation class ExtractMessage
 */
public class ExtractMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	private ExtractMessageLocal ejb;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExtractMessage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UUID uuid = UUID.fromString(request.getPathInfo().substring(1));
		byte[] msgContent = ejb.getMessageContent(uuid);
		response.setStatus(200);
		response.setContentType("Multipart/Related");
//		response.setContentLength(msgContent.length);
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(msgContent);
		outputStream.close();
	}

}
