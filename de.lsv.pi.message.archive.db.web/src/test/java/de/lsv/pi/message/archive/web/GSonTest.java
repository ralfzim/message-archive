package de.lsv.pi.message.archive.web;

import java.lang.reflect.Type;
import java.util.Arrays;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class GSonTest {

	public static class XX {
		boolean a;
		int[] b;
		@Override
		public String toString() {
			return "XX [a=" + a + ", b=" + Arrays.toString(b) + "]";
		}
		
	}
	
	@Test
	public void testList() {
		Gson gson = new Gson();
		String i = "[1,2,3,4]";
		int[] result = gson.fromJson(i, int[].class);
		System.out.println(Arrays.toString(result));
		String l = "{\"a\" : true,\"b\" : [2,3,4]}";
		Type typeOfT = TypeToken.get(XX.class).getType();
		Object r1 = gson.fromJson(l, typeOfT);
		System.out.println(r1);
	}
}
